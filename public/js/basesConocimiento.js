var RUTA_HELPS = ruta+'/basesconocimientos';


function cargar(cadena = ""){
	$('#btn-load').prop('disabled', true);
	$('#btn-load i').addClass('fa-pulse');
	$('#btn-load span').html('Cargando...');
	var RUTA_FULL = RUTA_HELPS;
	if(cadena == ""){
		RUTA_FULL+='/listar';
	}else{
		RUTA_FULL+='/listar/'+cadena;
	}
	$.ajax({
		url: RUTA_FULL,
		type: 'GET',
		dataType: 'json',
		success: function(data){
			console.log(data);
			var txt ="";
			if(data.length == 0){
				txt = '<div class="col-xs-12 text-center" style="margin-bottom: 10px"><h2>No se han encontrado Resultados</h2></div>'

			}else{
				for (var i = 0; i < data.length; i++) {
						
					txt+='<li>'+
					'<span class="text" style="margin-top: -25px;"><a href="'+RUTA_HELPS+'/'+data[i].slug+'"><h3>'+data[i].titulo+'</h3>'+
					'<small>Categoria: '+data[i].categoria_nombre;

					if(data[i].sub_categoria_nombre){
						txt+=' > '+data[i].sub_categoria_nombre;
					}
					txt+='</small></a></span>'+
					'<div class="tools">';

					if(data[i].rol_id == 3){
						txt+='<i class="fa fa-edit" OnClick="edit('+data[i].id+')"></i>'+
					'<i class="fa fa-trash-o" OnClick="eliminar('+data[i].id+')"></i>';
					}
					
					txt+='</div>'+
					'</li>';
				}
			}
			console.log(txt);
			$('#tabla-ayuda').fadeIn().html(txt);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#btn-load').prop('disabled', false);
			$('#btn-load i').removeClass('fa-pulse');
			$('#btn-load span').html('Actualizar');
		}
	});
}

cargar($('#busrcar-tema').val());

$('#busrcar-tema').keyup(function(e){
	var cadena = $(this).val();
	cargar(cadena);

});

function edit(id){
	window.location = RUTA_HELPS+'/'+id+'/edit';
}


function eliminar(id){

	swal({
		title: '¿Estás seguro que desea eliminar este Tema?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				$.ajax({
					url: RUTA_HELPS+"/"+id,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						cargar();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							jqXHR.responseJSON.message,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'Se ha elimando el tema de las Bases de Conocimiento con exito!',
			'success'
			);
	});

}