var RUTA_INCIDENTE = ruta+'/incidentecliente';
var tabla_incidente = $("#tabla-incidentes").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_INCIDENTE+'/listar',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){
			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ 
		data: 'problema', 
		name: 'problema',
		render: function(data, type, full, meta){
			var text = '<b>'+data+'</b><br>';
			if(full.descripcion.length > 160 )
			{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+' ...</p>';	
			}else{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+'</p>';
			}
			if(full.prioridad.nombre == "ALTA"){
				text+= '<p><b>Prioridad:</b> <span class="label label-danger">'+full.prioridad.nombre+'</span></p>';
			}

			if(full.prioridad.nombre == "MEDIA"){
				text+= '<p><b>Prioridad:</b> <span class="label label-warning">'+full.prioridad.nombre+'</span></p>';
			}

			if(full.prioridad.nombre == "BAJA"){
				text+= '<p><b>Prioridad:</b> <span class="label label-info">'+full.prioridad.nombre+'</span></p>';
			}


			
			return '<div style="width: 400px; word-break: break-all;">'+text+'</div>'
		}
	},
	{ data: 'solucion', name: 'solucion'},
	{ 
		data: 'responsable_id', 
		name: 'responsable_id',
		render: function(data, type, full){
			if(full.responsable != null){
				return full.responsable.name+' '+full.responsable.apellido
			}else{
				return "";
			}
		}
	},{ 
		data: 'estado_id', 
		name: 'estado_id',
		render: function(data, type, full){
			var text;
			if(full.estado.nombre == "ABIERTO"){
				text = '<span class="label label-success label-lg">'+full.estado.nombre+'</span>';
			}
			if(full.estado.nombre == "EN PROCESO"){
				text = '<span class="label label-primary label-lg">'+full.estado.nombre+'</span>';
			}
			if(full.estado.nombre == "CERRADO"){
				text = '<span class="label label-warning label-lg">'+full.estado.nombre+'</span>';
			}

			if(full.estado.nombre == "RESPONDIDO"){
				text = '<span class="label label-info label-lg bg-navy color-palette">'+full.estado.nombre+' <button type="button" class="btn btn-warning btn-xs bg-yellow color-palette" data-toggle="tooltip" data-placement="top" title="Calificar" OnClick="showAchivos('+full.id+', 2)"><i class="fa fa-star"></i></button></span>';
			}
			return '<div class="text-center">'+text+'</div>';
		}
	},
	{ data: 'created_at', name: 'created_at'},
	{ 
		data: 'id', 
		name: 'id',
		render: function(data, type, full){
			var text = "";
			if(full.archivos.length > 0){
				text = '<button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Archivos" OnClick="showAchivos('+data+', 1)"><i class="fa fa-file"></i></button>'
			}
			return '<div class="text-center">'+text+'</div>';
		}
	},
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


$('#actualizar').click(function(){
	tabla_incidente.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-incidentes').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_incidente.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-incidentes tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});




$('#modal-incidente').on('hidden.bs.modal', function (e) 
{
	$('#titulo-modal-incidente').html("");
	$('#contenido-modal-incidente').html("")
}); 


function showAchivos(id, option){
	$.ajax({
		url: RUTA_INCIDENTE+'/'+id,
		type: 'GET',
		success: function(res){ 
			// $('#id_departamento').val(res.id);
			// $('#nombre-departamento').val(res.nombre);
			// $('#descripcion-departamento').val(res.descripcion);
			// $('#padre-departamento').val(res.padre);
			var titulo;
			var contenido;
			if(option == 1){
				if(res.archivos.length > 0){
					titulo = 'Archivos del Incidente ID = '+res.id;
					contenido = '<table class="table table-hover table-bordered">';
					for (var i = 0; i < res.archivos.length; i++) {
						contenido+= '<tr><td>';
						contenido+= '<a href="'+ruta+'/uploads/archivo/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_publico+'</a>'
						contenido+= '</tr></td>';
					}
					contenido+= '<table>';
					$('#titulo-modal-incidente').html(titulo);
					$('#contenido-modal-incidente').html(contenido)
					$('#modal-incidente').modal('show');

				}	
			}
			if(option == 2){
				$('#id').val(res.id);
				$('#solucion').val(res.solucion);
				$('#modal-form-incidente').modal('show');
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la departamento. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

$('#modal-form-incidente').on('hidden.bs.modal', function (e) 
{
	$('#form-incidente')[0].reset();
	$('#id').val('');
	tabla_incidente.ajax.reload();
	removerStyle();
}); 

function Incidente(){
	this.id = $('#id').val();
	this.calificacion = $('input:radio[name=calificacion]:checked').val();
	
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removerStyle(){
	$('#field-solucion').removeClass("has-error");
	$('#field-solucion .msj-error').html("");
}

$('#guardar-incidente').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Incidente();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_INCIDENTE
	}else{
		type = 'PUT';
		route = RUTA_INCIDENTE+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removerStyle();
			$('#modal-form-incidente').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removerStyle()
				if(jqXHR.responseJSON.solucion){
					$('#field-solucion').addClass("has-error");
					$('#field-solucion .msj-error').html(jqXHR.responseJSON.solucion)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});

