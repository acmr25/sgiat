var RUTA_INCIDENTE_SOPORTE = ruta+'/incidentesoporte';
var tabla_incidente = $("#tabla-incidentes").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_INCIDENTE_SOPORTE+'/listar/3',
	columns: [
	{ data: 'id', name: 'id'},
	{ 
		data: 'problema', 
		name: 'problema',
		render: function(data, type, full, meta){
			var text = '<small><b>Cliente:</b> '+full.user.name+' '+full.user.apellido+'</small></br>';
			text+='<b><a href="javascript:void(0)" onclick="show('+full.id+', 3)">'+data+'</a></b>&nbsp&nbsp';
			if(full.archivos.length > 0){
				text+= '<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Archivos" OnClick="show('+full.id+', 1)"><i class="fa fa-file"></i></button>&nbsp&nbsp';
			}

			if(full.solucion == null || full.solucion == ""){
				text+= '<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Transferir" OnClick="tranferir('+full.id+')"><b>TRANSFERIR</b></button>&nbsp&nbsp';
			}


			if(full.descripcion.length > 160 )
			{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+' ...</p>';	
			}else{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+'</p>';
			}
			if(full.prioridad.nombre == "ALTA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-danger">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "MEDIA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-warning">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "BAJA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-info">'+full.prioridad.nombre+'</span></small></br>';
			}
			text += '<p><small><b>Fecha de creación: </b> '+full.fecha+'</small></p>';
			
			return '<div style="width: 100%; word-break: break-all;">'+text+'</div>'
		}
	},{
		data: 'solucion', 
		name: 'solucion',
	},{ 
		data: 'responsable_id', 
		name: 'responsable_id',
		render: function(data, type, full, meta){
			var text = full.responsable.name+' '+full.responsable.apellido;
			return text;
		}

	},
    { data: 'calificacion', name: 'calificacion'},
	{ data: 'cliente', name: 'cliente', "visible": false},
	{ data: 'descripcion', name: 'descripcion', "visible": false},
	{ data: 'categoria.nombre', name: 'categoria.nombre', "visible": false},
	{ data: 'prioridad.nombre', name: 'prioridad.nombre', "visible": false},
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});
$('#actualizar').click(function(){
	tabla_incidente.ajax.reload();
})