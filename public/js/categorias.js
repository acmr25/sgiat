var RUTA_CATEGORIAS = ruta+'/categorias';
var tabla = $("#tabla-categorias").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_CATEGORIAS+'/listar',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){

			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ 
		data: 'nombre', 
		name: 'nombre'
	},
	{ data: 'padre', name: 'padre'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showCategoria('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteCategoria('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


loadCatalago();
function loadCatalago(){
	$.getJSON(RUTA_CATEGORIAS+'/catalago',function(data){
		$("#padre-categoria").fadeIn(1000).html("");
		$("#padre-categoria").append('<option value="">- Seleccione -</option>');
		for (var i = 0; i < data.length ; i++) {
			$("#padre-categoria").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}

		$("#padre-categoria-select").fadeIn(1000).html("");
		$("#padre-categoria-select").append('<option value="">- Seleccione -</option>');
		for (var i = 0; i < data.length ; i++) {
			$("#padre-categoria-select").append('<option value="'+data[i].id+'">'+data[i].nombre+'</option>');
		}
	});
}


$('#actualizar').click(function(){
	tabla.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all').on('click', function(){
  // Get all rows with search applied
  var rows = tabla.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-categorias tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});

function Categoria(){
	this.id = $('#id_categoria').val();
	this.nombre = $('#nombre-categoria').val();
	this.descripcion = $('#descripcion-categoria').val();
	this.padre = $('#padre-categoria').val();
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleCategoria(){
	$('#field-nombre-categoria').removeClass("has-error");
	$('#field-nombre-categoria .msj-error').html("");
}

$('#guardar-categoria').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Categoria();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_CATEGORIAS
	}else{
		type = 'PUT';
		route = RUTA_CATEGORIAS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleCategoria();
			$('#modal-categoria').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleCategoria()
				if(jqXHR.responseJSON.nombre){
					$('#field-nombre-categoria').addClass("has-error");
					$('#field-nombre-categoria .msj-error').html(jqXHR.responseJSON.nombre)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-categoria').on('hidden.bs.modal', function (e) 
{
	tabla.ajax.reload();
	$('#form-categoria')[0].reset();
	$('#id_categoria').val('');
	removeStyleCategoria();
	loadCatalago();
}); 



function showCategoria(id){
	$.ajax({
		url: RUTA_CATEGORIAS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#id_categoria').val(res.id);
			$('#nombre-categoria').val(res.nombre);
			$('#descripcion-categoria').val(res.descripcion);
			$('#padre-categoria').val(res.padre);
			$('#modal-categoria').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la categoria. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteCategoria(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_CATEGORIAS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la categoria. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'La categoria se ha eliminado exitosamente',
			'success'
			);
	});

}

$('#select-delete-categoria').click(function(){

	var data = {
		ids: tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	};

	if(data.ids.length > 0){

		swal({
			title: '¿Estás seguro que desea eliminar los registros seleccionados?',
			text: "Esta acción no podra ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'No, cancalar',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					
					$.ajax({
						url: RUTA_CATEGORIAS+'/select',
						type: 'DELETE',
						data: data,
						headers: {'X-CSRF-TOKEN': $('#token').val()},
						success: function(res){ 
							resolve()
							tabla.ajax.reload();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							swal(
								'Error',
								'Ha ocurrido un error al tratar de eliminar los datos. Status: '+jqXHR.status,
								'error'
								)
						}
					})
				});
			},
			allowOutsideClick: false
		}).then(function() {
			swal(
				'Eliminado!',
				'Los registros seleccionados se ha eliminado exitosamente',
				'success'
				);
		});
	}else{
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}
});

$('#select-edit-categoria').click(function(){
	var ids = tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	if($.isEmptyObject(ids) == true){
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}else{
		$('#modal-categorias-select').modal('show');
	}
});



$('#modal-categorias-select').on('hidden.bs.modal', function (e) 
{
	tabla.ajax.reload();
	$('#form-categoria-select')[0].reset();
});


$('#guardar-categoria-select').click(function(){
	starLoad();
	var data = {
		ids : tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get(),
		padre: $('#padre-categoria-select').val()
	}
	
	$.ajax({
		url: RUTA_CATEGORIAS+'/select',
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: 'PUT',
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad()
			$('#modal-categorias-select').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han actualizados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad()
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
				'error'
				);	
		}
	});
});