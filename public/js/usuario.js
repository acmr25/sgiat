var RUTA_USUARIOS = ruta+'/usuarios';

$("[data-mask]").inputmask();


var tabla = $("#tabla-usuarios").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_USUARIOS+'/listar',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){

			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ data: 'name', name: 'name'},
	{ data: 'rol.nombre', name: 'rol.nombre'},
	{ data: 'departamento.nombre', name: 'departamento.nombre'},
	{ data: 'email', name: 'email'},
	{ data: 'telefono', name: 'telefono'},
	{ data: 'extension', name: 'extension'},
	{ data: 'cargo', name: 'cargo'},
	{ 
		data: 'estado', 
		name: 'estado',
		render: function(data){
			var text = "<b class='text-success'>Activo</b>";
			if(data == 0){
				var text = "<b class='text-danger'>Inactivo</b>";
			}
			return text;
		}
	},{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showusuario('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteusuario('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});



$('#actualizar').click(function(){
	tabla.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all').on('click', function(){
  // Get all rows with search applied
  var rows = tabla.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-usuarios tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});

function Usuario(){
	this.id = $('#id').val();
	this.name = $('#name').val();
	this.apellido = $('#apellido').val();
	this.email = $('#email').val();
	this.password = $('#password').val();
	this.password_confirmation = $('#password_confirmation').val();
	this.departamento_id = $('#id_departamento').val();
	this.cargo = $('#cargo').val();
	this.rol_id = $('#id_rol').val();
	this.telefono = $('#telefono').val();
	this.extension = $('#extension').val();
	this.estado = $('#estado').val();
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleusuario(){
	
	$('#field-nombre').removeClass("has-error");
	$('#field-nombre .msj-error').html("");

	$('#field-apellido').removeClass("has-error");
	$('#field-apellido .msj-error').html("");

	$('#field-email').removeClass("has-error");
	$('#field-email .msj-error').html("");

	$('#field-id_departamento').removeClass("has-error");
	$('#field-id_departamento .msj-error').html("");

	$('#field-cargo').removeClass("has-error");
	$('#field-cargo .msj-error').html("");

	$('#field-id_rol').removeClass("has-error");
	$('#field-id_rol .msj-error').html("");

	$('#field-password').removeClass("has-error");
	$('#field-password .msj-error').html("");

	$('#field-password_confirmation').removeClass("has-error");
	$('#field-password_confirmation .msj-error').html("");
}

$('#guardar-usuario').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Usuario();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_USUARIOS
	}else{
		type = 'PUT';
		route = RUTA_USUARIOS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleusuario();
			$('#modal-usuario').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleusuario()
				if(jqXHR.responseJSON.name){
					$('#field-nombre').addClass("has-error");
					$('#field-nombre .msj-error').html(jqXHR.responseJSON.name)
				}

				if(jqXHR.responseJSON.apellido){
					$('#field-apellido').addClass("has-error");
					$('#field-apellido .msj-error').html(jqXHR.responseJSON.apellido)
				}

				if(jqXHR.responseJSON.email){
					$('#field-email').addClass("has-error");
					$('#field-email .msj-error').html(jqXHR.responseJSON.email)
				}

				if(jqXHR.responseJSON.password){
					$('#field-password').addClass("has-error");
					$('#field-password .msj-error').html(jqXHR.responseJSON.password)
				}

				if(jqXHR.responseJSON.password_confirmation){
					$('#field-password_confirmation').addClass("has-error");
					$('#field-password_confirmation .msj-error').html(jqXHR.responseJSON.password_confirmation)
				}

				if(jqXHR.responseJSON.id_departamento){
					$('#field-id_departamento').addClass("has-error");
					$('#field-id_departamento .msj-error').html(jqXHR.responseJSON.id_departamento)
				}

				if(jqXHR.responseJSON.cargo){
					$('#field-cargo').addClass("has-error");
					$('#field-cargo .msj-error').html(jqXHR.responseJSON.cargo)
				}

				if(jqXHR.responseJSON.id_rol){
					$('#field-id_rol').addClass("has-error");
					$('#field-id_rol .msj-error').html(jqXHR.responseJSON.id_rol)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-usuario').on('hidden.bs.modal', function (e) 
{
	tabla.ajax.reload();
	$('#form-usuario')[0].reset();
	$('#id_usuario').val('');
	removeStyleusuario();
	
}); 



function showusuario(id){
	$.ajax({
		url: RUTA_USUARIOS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#id').val(res.id);
			$('#name').val(res.name);
			$('#apellido').val(res.apellido);
			$('#email').val(res.email);
			$('#cargo').val(res.cargo);
			$('#extension').val(res.extension);
			$('#telefono').val(res.telefono);
			$('#id_rol').val(res.rol_id);
			$('#id_departamento').val(res.departamento_id);
			$('#estado').val(res.estado);
			$('#modal-usuario').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la usuario. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteusuario(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_USUARIOS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la usuario. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'La usuario se ha eliminado exitosamente',
			'success'
			);
	});

}

$('#select-delete-usuario').click(function(){

	var data = {
		ids: tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	};

	if(data.ids.length > 0){

		swal({
			title: '¿Estás seguro que desea eliminar los registros seleccionados?',
			text: "Esta acción no podra ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'No, cancalar',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					
					$.ajax({
						url: RUTA_USUARIOS+'/select',
						type: 'DELETE',
						data: data,
						headers: {'X-CSRF-TOKEN': $('#token').val()},
						success: function(res){ 
							resolve()
							tabla.ajax.reload();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							swal(
								'Error',
								'Ha ocurrido un error al tratar de eliminar los datos. Status: '+jqXHR.status,
								'error'
								)
						}
					})
				});
			},
			allowOutsideClick: false
		}).then(function() {
			swal(
				'Eliminado!',
				'Los registros seleccionados se ha eliminado exitosamente',
				'success'
				);
		});
	}else{
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}
});

$('#select-edit-usuario').click(function(){
	var ids = tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	if($.isEmptyObject(ids) == true){
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}else{
		$('#modal-usuario-select').modal('show');
	}
});



$('#modal-usuario-select').on('hidden.bs.modal', function (e) 
{
	tabla.ajax.reload();
	$('#form-usuario-select')[0].reset();
});


$('#guardar-select').click(function(){
	starLoad();
	var data = {
		ids : tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get(),
		departamento_id: $('#departamento_id-select').val(),
		cargo: $('#cargo-select').val(),
		rol_id: $('#rol_id-select').val(),
		extension: $('#extension-select').val(),
		estado: $('#estado-select').val(),
	}
	
	$.ajax({
		url: RUTA_USUARIOS+'/select',
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: 'PUT',
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad()
			$('#modal-usuario-select').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han actualizados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad()
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
				'error'
				);	
		}
	});
});