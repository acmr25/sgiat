var RUTA_DEPARTAMENTOS = ruta+'/departamentos';
var tabla_departamento = $("#tabla-departamentos").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_DEPARTAMENTOS+'/listar',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){

			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ 
		data: 'nombre', 
		name: 'nombre'
	},
	{ data: 'descripcion', name: 'descripcion'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showdepartamento('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deletedepartamento('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


$('#actualizar-departamento').click(function(){
	tabla_departamento.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-departamentos').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_departamento.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-departamentos tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});

function Departamento(){
	this.id = $('#id_departamento').val();
	this.nombre = $('#nombre-departamento').val();
	this.descripcion = $('#descripcion-departamento').val();
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removeStyledepartamento(){
	$('#field-nombre-departamento').removeClass("has-error");
	$('#field-nombre-departamento .msj-error').html("");
}

$('#guardar-departamento').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Departamento();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_DEPARTAMENTOS
	}else{
		type = 'PUT';
		route = RUTA_DEPARTAMENTOS+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyledepartamento();
			$('#modal-departamento').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyledepartamento()
				if(jqXHR.responseJSON.nombre){
					$('#field-nombre-departamento').addClass("has-error");
					$('#field-nombre-departamento .msj-error').html(jqXHR.responseJSON.nombre)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-departamento').on('hidden.bs.modal', function (e) 
{
	tabla_departamento.ajax.reload();
	$('#form-departamento')[0].reset();
	$('#id_departamento').val('');
	removeStyledepartamento();
	loadCatalago();
}); 



function showdepartamento(id){
	$.ajax({
		url: RUTA_DEPARTAMENTOS+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#id_departamento').val(res.id);
			$('#nombre-departamento').val(res.nombre);
			$('#descripcion-departamento').val(res.descripcion);
			$('#padre-departamento').val(res.padre);
			$('#modal-departamento').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la departamento. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deletedepartamento(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_DEPARTAMENTOS+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_departamento.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la departamento. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'La departamento se ha eliminado exitosamente',
			'success'
			);
	});

}

$('#select-delete-departamento').click(function(){

	var data = {
		ids: tabla_departamento.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	};

	if(data.ids.length > 0){

		swal({
			title: '¿Estás seguro que desea eliminar los registros seleccionados?',
			text: "Esta acción no podra ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'No, cancalar',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					
					$.ajax({
						url: RUTA_DEPARTAMENTOS+'/select',
						type: 'DELETE',
						data: data,
						headers: {'X-CSRF-TOKEN': $('#token').val()},
						success: function(res){ 
							resolve()
							tabla_departamento.ajax.reload();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							swal(
								'Error',
								'Ha ocurrido un error al tratar de eliminar los datos. Status: '+jqXHR.status,
								'error'
								)
						}
					})
				});
			},
			allowOutsideClick: false
		}).then(function() {
			swal(
				'Eliminado!',
				'Los registros seleccionados se ha eliminado exitosamente',
				'success'
				);
		});
	}else{
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}
});

$('#select-edit-departamento').click(function(){
	var ids = tabla_departamento.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	if($.isEmptyObject(ids) == true){
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}else{
		$('#modal-departamentos-select').modal('show');
	}
});



$('#modal-departamentos-select').on('hidden.bs.modal', function (e) 
{
	tabla_departamento.ajax.reload();
	$('#form-departamento-select')[0].reset();
});


$('#guardar-departamento-select').click(function(){
	starLoad();
	var data = {
		ids : tabla_departamento.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get(),
		padre: $('#padre-departamento-select').val()
	}
	
	$.ajax({
		url: RUTA_DEPARTAMENTOS+'/select',
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: 'PUT',
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad()
			$('#modal-departamentos-select').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han actualizados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad()
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
				'error'
				);	
		}
	});
});