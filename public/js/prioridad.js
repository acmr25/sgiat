var RUTA_PRIORIDADES = ruta+'/prioridades';
var tabla_prioridad = $("#tabla-prioridades").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_PRIORIDADES+'/listar',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){

			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ 
		data: 'nombre', 
		name: 'nombre'
	},
	{ data: 'tiempo', name: 'tiempo'},
	{ data: 'descripcion', name: 'descripcion'},
	{
		data: 'id',
		'orderable': false,
		render: function ( data, type, full, meta ) {
			var text = '<div class="text-center">'+
			'<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="showprioridad('+data+')"><i class="fa fa-edit"></i></button>'+
			'<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteprioridad('+data+')"><i class="fa fa-remove"></i></button>'+
			
			'</div>';
			return text;
		}
	}
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});


$('#actualizar-prioridad').click(function(){
	tabla_prioridad.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-prioridades').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_prioridad.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-prioridads tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});

function prioridad(){
	this.id = $('#id_prioridad').val();
	this.nombre = $('#nombre-prioridad').val();
	this.tiempo = $('#tiempo-prioridad').val();
	this.descripcion = $('#descripcion-prioridad').val();
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleprioridad(){
	$('#field-nombre-prioridad').removeClass("has-error");
	$('#field-nombre-prioridad .msj-error').html("");

	$('#field-tiempo-prioridad').removeClass("has-error");
	$('#field-tiempo-prioridad .msj-error').html("");
}

$('#guardar-prioridad').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new prioridad();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_PRIORIDADES
	}else{
		type = 'PUT';
		route = RUTA_PRIORIDADES+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removeStyleprioridad();
			$('#modal-prioridad').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removeStyleprioridad()
				if(jqXHR.responseJSON.nombre){
					$('#field-nombre-prioridad').addClass("has-error");
					$('#field-nombre-prioridad .msj-error').html(jqXHR.responseJSON.nombre)
				}

				if(jqXHR.responseJSON.tiempo){
					$('#field-tiempo-prioridad').addClass("has-error");
					$('#field-tiempo-prioridad .msj-error').html(jqXHR.responseJSON.tiempo)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});


$('#modal-prioridad').on('hidden.bs.modal', function (e) 
{
	tabla_prioridad.ajax.reload();
	$('#form-prioridad')[0].reset();
	$('#id_prioridad').val('');
	removeStyleprioridad();
	loadCatalago();
}); 



function showprioridad(id){
	$.ajax({
		url: RUTA_PRIORIDADES+'/'+id,
		type: 'GET',
		success: function(res){ 
			$('#id_prioridad').val(res.id);
			$('#nombre-prioridad').val(res.nombre);
			$('#descripcion-prioridad').val(res.descripcion);
			$('#tiempo-prioridad').val(res.tiempo);
			$('#modal-prioridad').modal('show');
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la prioridad. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}

function deleteprioridad(id){

	swal({
		title: '¿Estás seguro que quiere eliminar?',
		text: "Esta acción no podra ser revertida!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'No, cancalar',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				var route =  RUTA_PRIORIDADES+"/"+id;
				$.ajax({
					url: route,
					type: 'DELETE',
					headers: {'X-CSRF-TOKEN': $('#token').val()},
					success: function(res){ 
						resolve()
						tabla_prioridad.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de eliminar la prioridad. Status: '+jqXHR.status,
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Eliminado!',
			'La prioridad se ha eliminado exitosamente',
			'success'
			);
	});

}

$('#select-delete-prioridad').click(function(){

	var data = {
		ids: tabla_prioridad.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	};

	if(data.ids.length > 0){

		swal({
			title: '¿Estás seguro que desea eliminar los registros seleccionados?',
			text: "Esta acción no podra ser revertida!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'No, cancalar',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					
					$.ajax({
						url: RUTA_PRIORIDADES+'/select',
						type: 'DELETE',
						data: data,
						headers: {'X-CSRF-TOKEN': $('#token').val()},
						success: function(res){ 
							resolve()
							tabla_prioridad.ajax.reload();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							swal(
								'Error',
								'Ha ocurrido un error al tratar de eliminar los datos. Status: '+jqXHR.status,
								'error'
								)
						}
					})
				});
			},
			allowOutsideClick: false
		}).then(function() {
			swal(
				'Eliminado!',
				'Los registros seleccionados se ha eliminado exitosamente',
				'success'
				);
		});
	}else{
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}
});

$('#select-edit-prioridad').click(function(){
	var ids = tabla_prioridad.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	if($.isEmptyObject(ids) == true){
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}else{
		$('#modal-prioridads-select').modal('show');
	}
});



$('#modal-prioridads-select').on('hidden.bs.modal', function (e) 
{
	tabla_prioridad.ajax.reload();
	$('#form-prioridad-select')[0].reset();
});


$('#guardar-prioridad-select').click(function(){
	starLoad();
	var data = {
		ids : tabla_prioridad.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get(),
		padre: $('#padre-prioridad-select').val()
	}
	
	$.ajax({
		url: RUTA_PRIORIDADES+'/select',
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: 'PUT',
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad()
			$('#modal-prioridads-select').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han actualizados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad()
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
				'error'
				);	
		}
	});
});