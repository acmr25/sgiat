var RUTA_INCIDENTE_SOPORTE = ruta+'/incidentesoporte';
var tabla_incidente = $("#tabla-incidentes").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_INCIDENTE_SOPORTE+'/listar/1',
	columns: [
	{
		'targets': 0,
		'searchable': false,
		'orderable': false,
		'className': 'text-center',
		'render': function (data, type, full, meta){
			return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
		}
	},
	{ data: 'id', name: 'id'},
	{ 
		data: 'problema', 
		name: 'problema',
		render: function(data, type, full, meta){
			var text = '<small><b>Cliente:</b> '+full.user.name+' '+full.user.apellido+'</small></br>';
			text+='<b><a href="javascript:void(0)" onclick="show('+full.id+', 2)">'+data+'</a></b>&nbsp&nbsp';
			if(full.archivos.length > 0){
				text+= '<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Archivos" OnClick="show('+full.id+', 1)"><i class="fa fa-file"></i></button>&nbsp&nbsp';
			}
			text+= '<button type="button" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Tomar Incidente" OnClick="tomar('+full.id+', 2)"><i class="fa fa-share"></i></button><br>'
			if(full.descripcion.length > 160 )
			{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+' ...</p>';	
			}else{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+'</p>';
			}
			if(full.prioridad.nombre == "ALTA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-danger">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "MEDIA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-warning">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "BAJA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-info">'+full.prioridad.nombre+'</span></small></br>';
			}
			text += '<p><small><b>Fecha de creación: </b> '+full.fecha+'</small></p>';
			
			return '<div style="width: 100%; word-break: break-all;">'+text+'</div>'
		}
	},
	{ data: 'cliente', name: 'cliente', "visible": false},
	{ data: 'descripcion', name: 'descripcion', "visible": false},
	{ data: 'categoria.nombre', name: 'categoria.nombre', "visible": false},
	{ data: 'prioridad.nombre', name: 'prioridad.nombre', "visible": false},
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});

$('#actualizar-incidentes').click(function(){
	tabla_incidente.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-incidentes').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_incidente.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-incidentes tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});

// ------------------------------------------------------------------------------------------------------------------------------------------------


var tabla_incidente_mios = $("#tabla-incidentes-mios").DataTable({
	processing: true,
	serverSide: true,
	language: leng,
	ajax: RUTA_INCIDENTE_SOPORTE+'/listar/2',
	columns: [
	// {
	// 	'targets': 0,
	// 	'searchable': false,
	// 	'orderable': false,
	// 	'className': 'text-center',
	// 	'render': function (data, type, full, meta){
	// 		return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
	// 	}
	// },
	{ data: 'id', name: 'id'},
	{ 
		data: 'problema', 
		name: 'problema',
		render: function(data, type, full, meta){
			var text = '<small><b>Cliente:</b> '+full.user.name+' '+full.user.apellido+'</small></br>';
			text+='<b><a href="javascript:void(0)" onclick="show('+full.id+', 3)">'+data+'</a></b>&nbsp&nbsp';
			if(full.archivos.length > 0){
				text+= '<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Archivos" OnClick="show('+full.id+', 1)"><i class="fa fa-file"></i></button>&nbsp&nbsp';
			}

			if(full.solucion == null || full.solucion == ""){
				text+= '<button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="Transferir" OnClick="tranferir('+full.id+')"><b>TRANSFERIR</b></button>&nbsp&nbsp';
			}


			if(full.descripcion.length > 160 )
			{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+' ...</p>';	
			}else{
				text+= '<p style="font-size: 12px">'+full.descripcion.substr(0,160)+'</p>';
			}
			if(full.prioridad.nombre == "ALTA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-danger">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "MEDIA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-warning">'+full.prioridad.nombre+'</span></small></br>';
			}

			if(full.prioridad.nombre == "BAJA"){
				text+= '<small><b>Prioridad:</b> <span class="label label-info">'+full.prioridad.nombre+'</span></small></br>';
			}
			text += '<p><small><b>Fecha de creación: </b> '+full.fecha+'</small></p>';
			
			return '<div style="width: 100%; word-break: break-all;">'+text+'</div>'
		}
	},{
		data: 'solucion', 
		name: 'solucion',
	},
	{ data: 'cliente', name: 'cliente', "visible": false},
	{ data: 'descripcion', name: 'descripcion', "visible": false},
	{ data: 'categoria.nombre', name: 'categoria.nombre', "visible": false},
	{ data: 'prioridad.nombre', name: 'prioridad.nombre', "visible": false},
	],
	order: [[ 1, "desc" ]],
	scrollY:  "500px",
	scrollCollapse: true,
});
$('#actualizar-incidentes-mios').click(function(){
	tabla_incidente_mios.ajax.reload();
})

// Handle click on "Select all" control
$('#select-all-incidentes-mios').on('click', function(){
  // Get all rows with search applied
  var rows = tabla_incidente_mios.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

$('#tabla-incidentes-mios tbody').on('change', 'input[type="checkbox"]', function(){
  // If checkbox is not checked
  if(!this.checked){
  	var el = $('#select-all').get(0);
	     // If "Select all" control is checked and has 'indeterminate' property
	     if(el && el.checked && ('indeterminate' in el)){
	        // Set visual state of "Select all" control 
	        // as 'indeterminate'
	        el.indeterminate = true;
	    }
	}
});
// ------------------------------------------------------------------------------------------------------------------------------------------------
$('input').prop('width', '10%');


$('#modal-incidente').on('hidden.bs.modal', function (e) 
{
	$('#titulo-modal-incidente').html("");
	$('#contenido-modal-incidente').html("")
}); 

$('#modal-form-incidente').on('hidden.bs.modal', function (e) 
{
	$('#form-incidente')[0].reset();
	$('#id').val('');
	tabla_incidente_mios.ajax.reload();
	removerStyle();
}); 

function Incidente(){
	this.id = $('#id').val();
	this.solucion = $('#solucion').val();
	
}

function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 

function removerStyle(){
	$('#field-solucion').removeClass("has-error");
	$('#field-solucion .msj-error').html("");

	$('#field-reporte_id').removeClass("has-error");
	$('#field-reporte_id .msj-error').html("");
}

$('#guardar-incidente').click(function(){
	var type = "";
	var route = "";
	var btn = this
	starLoad(btn)
	var data = new Incidente();
	if(data.id == "" || data.id == null || data.id == undefined){
		type = 'POST';
		route = RUTA_INCIDENTE_SOPORTE
	}else{
		type = 'PUT';
		route = RUTA_INCIDENTE_SOPORTE+'/'+data.id
	}

	$.ajax({
		url: route,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		type: type,
		dataType: 'json',
		data: data,
		success: function(res){
			endLoad(btn)
			removerStyle();
			$('#modal-form-incidente').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han guardados los datos de forma exitosa! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown){
			endLoad(btn)
			if(jqXHR.status == 422){
				removerStyle()
				if(jqXHR.responseJSON.solucion){
					$('#field-solucion').addClass("has-error");
					$('#field-solucion .msj-error').html(jqXHR.responseJSON.solucion)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
					'error'
					)
			}

		}
	});
});

function tranferir(id){
	$('#modal-form-incidente-transferir').modal('show');
	$('#id-transferir').val(id);
}


function tomar(id){

	swal({
		text: "¿Estás seguro que desea tomar este incidente?",
		type: 'info',
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Si',
		cancelButtonText: 'No',
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve, reject) {
				// var route =  RUTA_INCIDENTE_SOPORTE+"/tomar/"+id;
				$.ajax({
					url: RUTA_INCIDENTE_SOPORTE+"/tomar/"+id,
					type: 'GET',
					success: function(res){ 
						resolve()
						tabla_incidente.ajax.reload();
						tabla_incidente_mios.ajax.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						swal(
							'Error',
							'Ha ocurrido un error al tratar de tomar el incidente.',
							'error'
							)
					}
				})
			});
		},
		allowOutsideClick: false
	}).then(function() {
		swal(
			'Exito!',
			'Se ha pasado el incidente a su tabla de registros',
			'success'
			);
	});

}


function show(id, option){
	$.ajax({
		url: RUTA_INCIDENTE_SOPORTE+'/'+id,
		type: 'GET',
		success: function(res){ 
			var titulo;
			var contenido;
			if(option == 1){
				if(res.archivos.length > 0){
					titulo = 'Archivos del Incidente ID = '+res.id;
					contenido = '<table class="table table-hover table-bordered">';
					for (var i = 0; i < res.archivos.length; i++) {
						contenido+= '<tr><td>';
						contenido+= '<a href="'+ruta+'/uploads/archivo/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_publico+'</a>'
						contenido+= '</tr></td>';
					}
					contenido+= '<table>';
				}	
			}

			if(option == 2){
				titulo = 'Incidente ID = '+res.id;
				contenido = '<h5 class="titulo-incidente">Cliente:</h5> <p>'+res.user.name+' '+res.user.apellido+'</p>';
				contenido += '<h5 class="titulo-incidente">Problema:</h5> <p>'+res.problema+'<br><small>Categoria: <span class="label label-default">'+res.categoria.nombre+'</span></small></p>';
				contenido += '<h5 class="titulo-incidente">Descripción:</h5> <p>'+res.descripcion+'</p>';
			}

			if(option == 1 || option == 2){
				$('#titulo-modal-incidente').html(titulo);
				$('#contenido-modal-incidente').html(contenido)
				$('#modal-incidente').modal('show');
			}else{
				$('#cliente').val(res.user.name+' '+res.user.apellido);
				$('#id').val(res.id);
				$('#categoria').val(res.categoria_principal);
				$('#sub_categoria').val(res.sub_categoria);
				$('#problema').val(res.problema);
				$('#descripcion').val(res.descripcion);
				$('#prioridad').val(res.prioridad.nombre);
				$('#fecha').val(res.fecha);
				$('#solucion').val(res.solucion);
				var contenido = "";
				if(res.archivos.length > 0){
					contenido = '<table class="table table-hover table-bordered">';
					for (var i = 0; i < res.archivos.length; i++) {
						contenido+= '<tr><td>';
						contenido+= '<a href="'+ruta+'/uploads/archivo/'+res.archivos[i].nombre_interno+'" target="_back">'+res.archivos[i].nombre_publico+'</a>'
						contenido+= '</tr></td>';
					}
					contenido+= '<table>';
				}
				$('#archivos-incidente').html(contenido);
				$('#modal-form-incidente').modal('show');
			}
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
			sweetAlert(
				'Error',
				'Ha ocurrido un error al tratar de obtener los datos de la departamento. Status: '+jqXHR.status,
				'error'
				)
		}
	});
}


$('#btn-tomar-select').click(function(){

	var data = {
		ids: tabla_incidente.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
	};

	if(data.ids.length > 0){

		swal({
			text: "¿Estás seguro que desea tomar este incidente?",
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Si',
			cancelButtonText: 'No',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					
					$.ajax({
						url: RUTA_INCIDENTE_SOPORTE+'/tomarselect',
						type: 'PUT',
						data: data,
						headers: {'X-CSRF-TOKEN': $('#token').val()},
						success: function(res){ 
							resolve()
							tabla_incidente.ajax.reload();
							tabla_incidente_mios.ajax.reload();
						},
						error: function(jqXHR, textStatus, errorThrown) {
							swal(
								'Error',
								'Ha ocurrido un error al tratar de eliminar los datos. Status: '+jqXHR.status,
								'error'
								)
						}
					})
				});
			},
			allowOutsideClick: false
		}).then(function() {
			swal(
				'Eliminado!',
				'Los registros seleccionados se ha eliminado exitosamente',
				'success'
				);
		});
	}else{
		swal(
			'Atención!',
			'Debe de seleccionar un registro para porder realizar esta acción',
			'warning'
			);
	}
});

$('#modal-form-incidente-transferir').on('hidden.bs.modal', function (e) 
{
	$('#form-incidente-transferir')[0].reset();
	$('#id-transferir').val('');
	tabla_incidente_mios.ajax.reload();
	removerStyle();
}); 


$('#guardar-incidente-tranferencia').click(function(){
	var btn = this;
	
	var data = {'id': $('#id-transferir').val(), 'responsable_id': $('#reporte_id-transferir').val()}

	$.ajax({
		url: RUTA_INCIDENTE_SOPORTE+"/transferencia/",
		type: 'PUT',
		data: data,
		headers: {'X-CSRF-TOKEN': $('#token').val()},
		success: function(res){ 
			
			endLoad(btn)
			tabla_incidente_mios.ajax.reload();
			$('#modal-form-incidente-transferir').modal('hide');
			sweetAlert(
				'Exito!',
				'Se han transferido el incidente con exito! ',
				'success'
				)
		},
		error: function(jqXHR, textStatus, errorThrown) {
			endLoad(btn)
			
			if(jqXHR.status == 422){
				removerStyle()
				if(jqXHR.responseJSON.responsable_id){
					$('#field-reporte_id').addClass("has-error");
					$('#field-reporte_id .msj-error').html(jqXHR.responseJSON.responsable_id)
				}
			}else{
				sweetAlert(
					'Error',
					'Ha ocurrido un error al tratar de transferir el incidente.',
					'error'
					)
			}

		}
	})
	
});