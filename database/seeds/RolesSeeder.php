<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Rol::create([
    		'nombre' => 'Administrador'
    		]);

    	Rol::create([
    		'nombre' => 'Cliente'
    		]);

    	Rol::create([
    		'nombre' => 'Soporte'
    		]);
    }
}
