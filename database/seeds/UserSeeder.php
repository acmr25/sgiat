<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name'=> 'Adrian',
        	'apellido' => 'Vega',
        	'cargo' => 'Soporte',
        	'email' => 'pasanteait@amazonastech.com.ve',
        	'telefono' => '04148992307',
        	'extension' => '124',
        	'password' => bcrypt(1234),
        	'rol_id' => 3,
        	'departamento_id' => 1,
        	'estado' => 1
        ]);
    }
}
