<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\EstadoIncidente;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoIncidente::create([
        	'nombre'=>'ABIERTO'
        	]);

        EstadoIncidente::create([
        	'nombre'=>'EN PROCESO'
        	]);

       	EstadoIncidente::create([
        	'nombre'=>'CERRADO'
        	]);

        EstadoIncidente::create([
            'nombre'=>'RESPONDIDO'
            ]);

    }
}
