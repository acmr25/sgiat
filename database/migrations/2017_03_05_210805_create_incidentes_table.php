<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->text('problema');
            $table->text('solucion');
            $table->timestamp('fecha_cierre');
            $table->integer('user_id')->unsigned();
            $table->integer('responsable_id')->unsigned()->nullable();
            $table->integer('categoria_id')->unsigned();
            $table->integer('prioridad_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('responsable_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('categoria_id')->references('id')->on('categorias_incidentes');
            $table->foreign('prioridad_id')->references('id')->on('prioridades');
            $table->foreign('estado_id')->references('id')->on('estados_incidentes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incidentes');
    }
}
