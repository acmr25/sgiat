<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignBasesConocimientoCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bases_conocimientos', function (Blueprint $table) {
            $table->integer('categoria_id')->unsigned()->after('descripcion');
            $table->foreign('categoria_id')->references('id')->on('categorias_incidentes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bases_conocimientos', function (Blueprint $table) {
            //
        });
    }
}
