<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('apellido');
            $table->string('cargo');
            $table->string('email')->unique();
            $table->string('telefono');
            $table->string('extension');
            $table->string('password', 60);
            $table->rememberToken();
            $table->integer('rol_id')->unsigned();
            $table->integer('departamento_id')->unsigned();
            $table->foreign('rol_id')->references('id')->on('roles');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
