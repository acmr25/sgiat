<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SGIAT| Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body class="hold-transition login-page" style="background-color: #00A65A">
       <div class="login-box">
        <div class="login-logo">
         <a href="{{ url() }}">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <img src="{{ asset('img/logo.png') }}" class="img-responsive" alt="Image">
            </div>
            <div class="col-sm-12">
              <h4 style="color: white; text-transform: uppercase;">Sistema de Gestión de Incidentes</h4>
              <h5 style="color: white; text-transform: uppercase;">Amazonas Tech C.A.</h5>
            </div>
          </div>
        </a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
       <p class="login-box-msg">Reseteo de Contraseña</p>
       @include('flash::message')
       {!! Form::open(['route'=>'password.email','method'=>'post']) !!}
       <div class="form-group has-feedback {{ $errors->has('email')?' has-error':'' }}">
        <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if($errors->has('email'))
        <div>
          <strong class="text-danger">{{ $errors->first('email')}}</strong>
        </div>
        @endif
      </div>

      <div class="row">

        <div class="col-xs-12"> 
          <hr>
        </div>


        <div class="col-sm-6 text-center">
          <a href="{{ route('auth.login')}}">Login</a>
        </div>
        <div class="col-sm-6 text-center">
          <a href="{{ route('auth.register')}}">Registrar</a>
        </div>


        <div class="col-xs-12"> 
          <hr>
        </div>

        <div class="col-xs-12">
          {!! Form::submit('Resetear Contraseña',['class'=>'btn btn-success btn-sm btn-block btn-flat']) !!}
        </div>

      </div>
      {!!Form::close()!!}

    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->

  <!-- jQuery 2.1.4 -->
  <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- iCheck -->
  <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script>
    $(function () {
     $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
   });
 </script>
</body>
</html>
