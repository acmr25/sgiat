<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SGIAT| Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page" style="background-color: #00A65A">
    	<div class="login-box">
    		<div class="login-logo">
    			<a href="{{ url() }}">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <img src="{{ asset('img/logo.png') }}" class="img-responsive" alt="Image">
                        </div>
                        <div class="col-sm-12">
                            <h4 style="color: white; text-transform: uppercase;">Sistema de Gestión de Incidentes</h4>
                            <h5 style="color: white; text-transform: uppercase;">Amazonas Tech C.A.</h5>
                        </div>
                    </div>
                </a>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
             <p class="login-box-msg">Login</p>

             {!! Form::open(['route'=>'auth.login','method'=>'post']) !!}
             <div class="form-group has-feedback {{ $errors->has('email')?' has-error':'' }}">
                {!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email']) !!}
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if($errors->has('email'))
                <i class="text-danger">
                   <strong><i class="fa fa-times-circle-o"></i>
                      @if($errors->first('email') == "These credentials do not match our records.")
                      Los datos ingresados no coinciden con nuestros registros
                      @else
                      {{ $errors->first('email')}}
                      @endif
                  </strong>
              </i>
              @endif
          </div>
          <div class="form-group has-feedback {{ $errors->has('password')?' has-error':'' }}">
            <input type="password" class="form-control" placeholder="Password" name="password" id="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if($errors->has('password'))
            <i class="text-danger">
               <strong><i class="fa fa-times-circle-o"></i>{{ $errors->first('password')}}</strong>
           </i>
           @endif
       </div>
       <div class="row">
        <div class="col-xs-8">
           <div class="checkbox icheck">
              <label>
                 <input type="checkbox"> Recordarme
             </label>
         </div>
     </div><!-- /.col -->
     <div class="col-xs-4">
       <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
   </div><!-- /.col -->
</div>
{!! Form::close() !!}

<a href="{{ route('password.email') }}">Olvidé mi contraseña</a><br>
<a href="{{ url('register') }}" class="text-center">Registrarme</a>

</div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
     $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
      });
 });
</script>
</body>
</html>
