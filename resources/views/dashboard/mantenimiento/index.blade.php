@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('titulo_modulo', "Modulo de Mantenimiento")

@section('contenido')
<div class="row">
	<div class="col-sm-6">
		@include('dashboard.categorias.index')
		@include('dashboard.prioridades.index')
	</div>
	<div class="col-sm-6">
		@include('dashboard.departamentos.index')
	</div>
</div>

@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/categorias.js')}}"></script>
<script src="{{asset('js/departamento.js')}}"></script>
<script src="{{asset('js/prioridad.js')}}"></script>
@endsection