<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Prioridades</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">

		<a class="btn btn-default btn-sm"  data-toggle="modal" href='#modal-prioridad'><i class="fa fa-plus"></i> Agregar prioridad</a>
		<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar-prioridad"><i class="fa fa-repeat"></i> Actualizar tabla</a>
		<hr>
		<table class="table table-bordered table-hover table-striped" id="tabla-prioridades" width="100%">
			<thead>
				<th  style="width: 32px; padding-right: 8px;">
					<input name="select_all" value="1" id="select-all-prioridades" type="checkbox">
				</th>
				<th>ID</th>
				<th>Priodidad</th>
				<th>Tiempo</th>
				<th>Descripcion</th>
				<th>Acción</th>
			</thead>
		</table>
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button class="btn btn-danger btn-sm" id="select-delete-prioridad">Eliminar Seleccionados</button>
	</div><!-- /.box-footer-->
</div><!-- /.box -->

@include('dashboard.prioridades.form')
{{-- @include('dashboard.categorias.formSelect') --}}