<!-- Modal -->
<div class="modal fade" id="modal-prioridad" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">
			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-prioridad']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Formulario de prioridades</h4>
			</div>
			<div class="modal-body">

				{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}
				{!! Form::hidden('id', null, ['id'=>'id_prioridad']) !!} 

				<div class="form-group" id="field-nombre-prioridad">
					{!! Form::label('nombre', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::text('nombre', null, ['id'=>'nombre-prioridad', 'class'=>'form-control','placeholder'=>'Nombre de la prioridad','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div>

				<div class="form-group" id="field-tiempo-prioridad">
					{!! Form::label('tiempo', 'Tiempo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::text('tiempo', null, ['id'=>'tiempo-prioridad', 'class'=>'form-control','placeholder'=>'Timpo de respuesta','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div>

				<div class="form-group" id="field-descripcion-prioridad">
					{!! Form::label('descripcion', 'Descripción:',['class'=>'control-label']) !!}
					{!!Form::textarea('descripcion', null, ['id'=>'descripcion-prioridad', 'class'=>'form-control','rows'=>'3','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div>  

			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-prioridad" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>