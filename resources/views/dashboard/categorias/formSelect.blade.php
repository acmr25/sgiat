<!-- Modal -->
<div class="modal fade" id="modal-categorias-select" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">
			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-categoria-select']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Formulario de Categorias</h4>
			</div>
			<div class="modal-body">

				{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}
 

				<div class="form-group" id="field-padre-categoria-select">
					{!!Form::label('padre', 'Categoria Superior:',['class'=>'control-label']) !!}
					{!!Form::select('padre', [], null, ['id'=>'padre-categoria-select', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div> 

			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-categoria-select" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
