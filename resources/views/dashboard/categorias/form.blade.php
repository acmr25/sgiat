<!-- Modal -->
<div class="modal fade" id="modal-categoria" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">
			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-categoria']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Formulario de Categorias</h4>
			</div>
			<div class="modal-body">

				{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}
				{!! Form::hidden('id', null, ['id'=>'id_categoria']) !!} 

				<div class="form-group" id="field-nombre-categoria">
					{!! Form::label('nombre', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::text('nombre', null, ['id'=>'nombre-categoria', 'class'=>'form-control','placeholder'=>'Nombre de la categoria','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div> 

				<div class="form-group" id="field-descripcion-categoria">
					{!! Form::label('descripcion', 'Descripción:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::textarea('descripcion', null, ['id'=>'descripcion-categoria', 'class'=>'form-control','rows'=>'3','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div> 

				<div class="form-group" id="field-padre-categoria">
					{!!Form::label('padre', 'Categoria Superior:',['class'=>'control-label']) !!}
					{!!Form::select('padre', [], null, ['id'=>'padre-categoria', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div> 

			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-categoria" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


