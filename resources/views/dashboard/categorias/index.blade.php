<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Categorias</h3>
		<div class="box-tools pull-right">
			<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="box-body">

		<a class="btn btn-default btn-sm"  data-toggle="modal" href='#modal-categoria'><i class="fa fa-plus"></i> Crear nueva categoria</a>
		<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar"><i class="fa fa-repeat"></i> Actualizar tabla</a>
		<hr>
		<table class="table table-bordered table-hover table-striped" id="tabla-categorias" width="100%">
			<thead>
				<th  style="width: 32px; padding-right: 8px;">
					<input name="select_all" value="1" id="select-all" type="checkbox">
				</th>
				<th style="width: 30px;">ID</th>
				<th>Categoria</th>
				<th>Categoria Padre</th>
				<th style="width: 50px;">Acción</th>
			</thead>
		</table>
	</div><!-- /.box-body -->
	<div class="box-footer">
		<button class="btn btn-danger btn-sm" id="select-delete-categoria">Eliminar Seleccionados</button>
		<button class="btn btn-default btn-sm" id="select-edit-categoria">Editar Seleccionados</button>
	</div><!-- /.box-footer-->
</div><!-- /.box -->

@include('dashboard.categorias.form')
@include('dashboard.categorias.formSelect')