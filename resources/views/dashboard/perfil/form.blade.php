
@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('titulo_modulo', "Perfil")

@section('contenido')
<div class="row">
	<div class="col-sm-6 col-sm-offset-3">
		<div class="box box-solid">
			
			<div class="box-body">
				<div class="col-xs-12">@include('flash::message')</div>	
				{!! Form::open(['route'=>['perfil.update',$user],'method'=>'POST']) !!}

				<div class="row">
					<div class="form-group col-sm-6 {{ $errors->has('name')?' has-error':'' }}" id="field-nombre">
						{!! Form::label('name', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('name', $user->name, ['id'=>'name', 'class'=>'form-control','placeholder'=>'Nombre','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						@if($errors->has('name'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('name')}}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-6 {{ $errors->has('apellido')?' has-error':'' }}" id="field-apellido">
						{!! Form::label('apellido', 'Apellido:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('apellido', $user->apellido, ['id'=>'apellido', 'class'=>'form-control','placeholder'=>'Apellido','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						@if($errors->has('apellido'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('apellido')}}</strong>
						</span>
						@endif
					</div>
				</div>

				<div class="form-group {{ $errors->has('email')?' has-error':'' }}" id="field-email">
					{!! Form::label('email', 'Email:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::email('email', $user->email, ['id'=>'email', 'class'=>'form-control','placeholder'=>'example@example.com','required'])!!}
					@if($errors->has('email'))
					<span class="text-danger">
						<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('email')}}</strong>
					</span>
					@endif
				</div> 

				<div class="row">
					<div class="form-group col-sm-6 {{ $errors->has('departamento_id')?' has-error':'' }}" id="field-departamento_id">
						{!! Form::label('departamento_id', 'Departamento:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::select('departamento_id', $deparatmentos, $user->departamento_id,['id'=>'departamento_id', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
						@if($errors->has('departamento_id'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('departamento_id')}}</strong>
						</span>
						@endif
					</div> 

					<div class="form-group col-sm-6 {{ $errors->has('cargo')?' has-error':'' }}" id="field-cargo">
						{!! Form::label('cargo', 'Cargo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('cargo', $user->cargo, ['id'=>'cargo', 'class'=>'form-control','placeholder'=>'Cargo','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						@if($errors->has('cargo'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('cargo')}}</strong>
						</span>
						@endif
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-6 {{ $errors->has('telefono')?' has-error':'' }}" id="field-telefono">
						{!! Form::label('telefono', 'Telefono:',['class'=>'control-label']) !!}
						{!!Form::text('telefono', $user->telefono, ['id'=>'telefono', 'class'=>'form-control','placeholder'=>'Telefono','data-inputmask'=>"'mask': '9999 999 9999'",'data-mask'])!!}
						@if($errors->has('telefono'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('telefono')}}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-6 {{ $errors->has('extension')?' has-error':'' }}" id="field-extension">
						{!! Form::label('extension', 'Extensión:',['class'=>'control-label']) !!}
						{!!Form::text('extension', $user->extension, ['id'=>'extension', 'class'=>'form-control','placeholder'=>'Extensión','data-inputmask'=>"'mask': '999'",'data-mask'])!!}
						@if($errors->has('extension'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('extension')}}</strong>
						</span>
						@endif
					</div>

				</div>

				<div class="row">
					<div class="form-group col-sm-6 {{ $errors->has('password')?' has-error':'' }}" id="field-password">
						{!! Form::label('password', 'Contraseña:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!! Form::password('password', ['id'=>'password', 'class'=>'form-control', 'placeholder'=>'*********']) !!}
						@if($errors->has('password'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('password')}}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-6 {{ $errors->has('password_confirmation')?' has-error':'' }}" id="field-password_confirmation">
						{!! Form::label('password_confirmation', 'Confirmar Contraseña:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!! Form::password('password_confirmation', ['id'=>'password_confirmation', 'class'=>'form-control', 'placeholder'=>'*********']) !!}
						@if($errors->has('password_confirmation'))
						<span class="text-danger">
							<strong><i class="fa fa-times-circle-o"></i> {{ $errors->first('password_confirmation')}}</strong>
						</span>
						@endif
					</div>
				</div>


			</div><!-- /.box-body -->
			<div class="box-footer">
				
				{!! Form::submit('Guardar cambios',['class'=>'btn btn-info btn-block']) !!}

			</div><!-- /.box-footer-->
			{!! Form::close() !!}
		</div><!-- /.box -->


	</div>
</div>
@include('dashboard.usuarios.form')
@include('dashboard.usuarios.formSelect')
@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{asset('js/validate.js')}}"></script>
@endsection