
<div class="modal fade" id="modal-incidente">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="titulo-modal-incidente"></h4>
			</div>
			<div class="modal-body" id="contenido-modal-incidente">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>