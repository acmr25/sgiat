@extends('dashboard.templates.main')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

@endsection

@section('title_page', (isset($incidente->id)?'Editar Ticket ('.$incidente->problema.')':'Nuevo Ticket') )

@section('breadcrumb')
<li><a href="{{route('incidentecliente.index')}}">Incidentes</a></li>
<li class="active">Nuevo Ticket</li>
@endsection 

@section('contenido')
<section class="content-fluid">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="box box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Nuevo Ticket</h3>
				</div>
				<div class="box-body pad">
					@if(isset($incidente->id))
					{!! Form::open(['route'=>['incidentecliente.update',$incidente],'method'=>'PUT', 'files'=>true, 'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
					{!! Form::hidden('id', $incidente->id) !!}  
					@else
					{!! Form::open(['route'=>'incidentecliente.store','method'=>'POST', 'files'=>true, 'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}  
					@endif
					<br>
					{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}


					<div class="form-group{{ $errors->has('categoria_id')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('categoria_id', 'Categoria:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::select('categoria_id', $categorias ,(isset($incidente)?$incidente->categoria_id:null), ['id'=>'categoria_id', 'class'=>'form-control', 'placeholder'=>'-Seleccione-','required']) !!}
							@if($errors->has('categoria_id'))
							<span class="text-danger">
								<strong>{{ $errors->first('categoria_id')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div id="box-sub-categorias">
						
					</div>


					<div class="form-group{{ $errors->has('problema')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('problema', 'Problema:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::text('problema', (isset($incidente)?$incidente->problema:null), ['id'=>'problema', 'class'=>'form-control', 'placeholder'=>'Problema','required']) !!}
							@if($errors->has('problema'))
							<span class="text-danger">
								<strong>{{ $errors->first('problema')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('descripcion')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('descripcion', 'Descripción:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::textarea('descripcion', (isset($incidente)?$incidente->prefijo_telefonico:null), ['id'=>'descripcion', 'class'=>'form-control', 'placeholder'=>'Descripcion','required', 'rows'=>3]) !!}
							@if($errors->has('descripcion'))
							<span class="text-danger">
								<strong>{{ $errors->first('descripcion')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('archivos')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('archivos', 'Archivo(s):',['class'=>'control-label']) !!}
							</div>
						</div>
						<div class="col-md-9">
							<input type="file" name="archivos[]" id='archivos' multiple="">
							@if($errors->has('archivos'))
							<span class="text-danger">
								<strong>{{ $errors->first('archivos')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('prioridad_id')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('prioridad_id', 'Prioridad:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::select('prioridad_id', $prioridades ,(isset($incidente)?$incidente->prioridad_id:null), ['id'=>'prioridad_id', 'class'=>'form-control','required']) !!}
							@if($errors->has('prioridad_id'))
							<span class="text-danger">
								<strong>{{ $errors->first('prioridad_id')}}</strong>
							</span>
							@endif
						</div>
					</div>

					
					<hr>

					<div class="form-group">
						<div class="col-md-12">
							<a href="{{route('incidentecliente.index')}}" class="btn btn-warning btn-sm">Cancelar</a>
							{!! Form::submit('Guardar Cambios',['class'=>'btn btn-primary btn-sm pull-right']) !!}
						</div>
					</div>
					{!! Form::close() !!} 
				</div>
			</div>
		</div>
		
	</div>
</section>


@endsection

@section('js')

<script src="{{asset('plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>


<script type="text/javascript">

	$('#archivos').fileinput({
		showUpload: false,
		language: 'es'
	});

	function soloLetras(e){
		key = e.keyCode || e.which;
		tecla = String.fromCharCode(key).toLowerCase();
		letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
		especiales = "8-37-39-46";

		tecla_especial = false
		for(var i in especiales){
			if(key == especiales[i]){
				tecla_especial = true;
				break;
			}
		}

		if(letras.indexOf(tecla)==-1 && !tecla_especial){
			return false;
		}
	}

	$('#categoria_id').change(function(){
		
		if($(this).val() != 0){
			$.ajax({
				url: ruta+'/categorias-by-padre/'+$(this).val(),
				type: 'GET',
				success: function(res){ 
					var html = '<div class="form-group">'+
					'<div class="col-md-3">'+
					'<div class="pull-right">'+
					'<label class="control-label">Sub-categoria: <span style="color:red;"> *</span></label>'+
					'</div>'+
					'</div>'+
					'<div class="col-md-9">'+
					'<select name="sub_categoria" id="sub_categoria" class="form-control">';

					for(var i = 0; i<res.length; i++){
						html+='<option value="'+res[i].id+'">'+res[i].nombre+'</opction>';
					}

					html+='</select>'+
					'</div>'+
					'</div>';

					$('#box-sub-categorias').append(html);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					sweetAlert(
						'Error',
						'Ha ocurrido un error al tratar de obtener los datos de la categoria. Status: '+jqXHR.status,
						'error'
						)
				}
			});
		}else{
			$('#box-sub-categorias').html("");
		}
		
	});

</script>
@endsection