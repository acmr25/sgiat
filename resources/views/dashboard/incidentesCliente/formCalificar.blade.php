<!-- Modal -->
<div class="modal fade" id="modal-form-incidente" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">

			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-incidente']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Calificación de Incidente</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-sm-12" id="field-id">
						{!! Form::label('id', 'ID:',['class'=>'control-label']) !!}
						{!!Form::text('id', null, ['id'=>'id', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-12" id="field-solucion">
						{!! Form::label('solucion', 'Solución:',['class'=>'control-label']) !!}
						{!!Form::textarea('solucion', null, ['id'=>'solucion', 'class'=>'form-control', 'rows'=>2, 'readonly'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>
				<hr>
		
				<div class="row">
					<div class="form-group col-sm-12">
						<h3>Califique el servicio</h3>

						<div class="radio">
							<label>
								<input name="calificacion" id="optionsRadios1" value="Excelente" checked="" type="radio">
								Excelente
							</label>&nbsp&nbsp

							<label>
								<input name="calificacion" id="optionsRadios2" value="Bueno" type="radio">
								Bueno
							</label>&nbsp&nbsp

							<label>
								<input name="calificacion" id="optionsRadios2" value="Regular" type="radio">
								Regular
							</label>&nbsp&nbsp

							<label>
								<input name="calificacion" id="optionsRadios2" value="Malo" type="radio">
								Malo
							</label>&nbsp&nbsp
						</div>
					</div>
				</div>
				


			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-incidente" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>