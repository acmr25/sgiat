@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('titulo_modulo', "Incidentes")

@section('contenido')
<div class="row">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	<div class="col-sm-12">
		<div class="box box-solid">
			
			<div class="box-body">

				<a class="btn btn-default btn-sm"  data-toggle="modal" href='{{ route('incidentecliente.create') }}'><i class="fa fa-plus"></i> Abrir nuevo ticket</a>
				<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar"><i class="fa fa-repeat"></i> Actualizar tabla</a>
				<hr>
				<table class="table table-bordered table-hover table-striped" id="tabla-incidentes" width="100%">
					<thead>
						<th  style="width: 32px; padding-right: 8px;">
							<input name="select_all" value="1" id="select-all" type="checkbox">
						</th>
						<th style="width: 30px;">ID</th>
						{{-- aqui ira descripcion, problema, categoria y prioridad--}}
						<th style="width: 35%">Ticket</th> 
						<th>Solución</th>
						<th>Responsable</th>
						<th>Estado</th>
						<th>Fecha de creacion</th>
						<th style="width: 50px;">Archivos</th>
					</thead>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer">
			</div><!-- /.box-footer-->
		</div><!-- /.box -->


	</div>
</div>
@include('dashboard.incidentesCliente.window')
@include('dashboard.incidentesCliente.formCalificar')



@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{asset('js/validate.js')}}"></script>
<script src="{{asset('js/indicentesCliente.js')}}"></script>
@endsection