@extends('dashboard.templates.main')

@section('css')
<style type="text/css">
	.small-box .icon {
		font-size: 70px;
	}

	.small-box:hover .icon {
		font-size: 75px;
	}
</style>
@endsection

@section('titulo_modulo', "")

@section('contenido')
<div class="row">
	<div class="col-xs-12 text-center">
		<div class="box box-default box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Hola {{ Auth::user()->name}}</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				Bienvenid@ al Sistema de Gestión de Incidentes de Amazonas Tech
			</div><!-- /.box-body -->
		</div>
	</div>

	@if(Auth::user()->rol_id != 2)
	<div class="col-md-12" >
		<!-- AREA CHART -->
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Estadísticas de Incidentes</h3>
				<div class="box-tools" style="width: 300px;">
					{{-- {!! Form::open() !!} --}}
					<div class="input-group pull-right" >

						{!! Form::select('mes', $meses, date('m'), ["id"=>"mes", 'class' => "form-control input-sm", 'style'=> "width:55%; margin-right: 5px"]) !!}	
						
						{!! Form::select('anio', $anios, date('Y'), ["id"=>"anio", 'class' => "form-control input-sm", 'style'=> "width:40%;"]) !!}						
						<div class="input-group-btn">
							<a href="javascript:void(0)" type="button" onclick="chartUpdate()" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Actualizar Grafica"><i class="fa fa-refresh" aria-hidden="true"></i>
							</a>
						</div>
						<button class="btn btn-sm btn-default pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
					
					{{-- {!! Form::close() !!} --}}
				</div>
			</div>
			<div class="box-body">

				<div class="row">

					<div class="col-md-2">	
						<div class="small-box bg-aqua-active">
							<div class="inner">
								<h3 id="total-incidentes" style="font-size: 25px">0</h3>
								<h5 style="font-weight: bold;">Total de Incidentes</h5>
							</div>
							<div class="icon">
								<i class="fa fa-database"></i>
							</div>
						</div>
					</div>

					<div class="col-md-2">	
						<div class="small-box bg-yellow-active color-palette ">
							<div class="inner">
								<h3 id="total-incidentes-cerrados" style="font-size: 25px">0</h3>
								<h5 style="font-weight: bold;">Incidentes cerrados</h5>
							</div>
							<div class="icon">
								<i class="fa fa-ticket"></i>
							</div>
						</div>
					</div>	

					<div class="col-md-2">	
						<div class="small-box bg-red-active color-palette">
							<div class="inner">
								<h3 id="total-incidentes-falta" style="font-size: 25px">0</h3>
								<h5 style="font-weight: bold;">Incidentes sin resolver</h5>
							</div>
							<div class="icon">
								<i class="fa fa-ticket"></i>
							</div>
						</div>
					</div>

					<div class="col-md-2">	
						<div class="small-box bg-light-blue-active color-palette">
							<div class="inner">
								<h3 id="total-incidentes-proceso" style="font-size: 25px">0</h3>
								<h5 style="font-weight: bold;">Incidentes en proceso</h5>
							</div>
							<div class="icon">
								<i class="fa fa-ticket"></i>
							</div>
						</div>
					</div>

					<div class="col-md-2">	
						<div class="small-box bg-green-active color-palette">
							<div class="inner">
								<h3 id="total-incidentes-mes" style="font-size: 25px">0</h3>
								<h5 style="font-weight: bold;">Incidentes en el mes</h5>
							</div>
							<div class="icon">
								<i class="fa fa-ticket"></i>
							</div>
						</div>
					</div>
				</div>

				<div class="chart">
					<canvas id="chart-incidentes" style="height: 400px; width: 510px;" width="510" height="250"></canvas>
				</div>
			</div><!-- /.box-body -->
		</div><!-- /.box -->

	</div>


	<div class="col-md-6" >
		<!-- AREA CHART -->
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Incidentes por Departamentos al Mes</h3>
				<div class="box-tools" style="width: 225px;">
					{{-- {!! Form::open() !!} --}}
					<div class="input-group pull-right" >

						{!! Form::select('mes_departamento', $meses, date('m'), ["id"=>"mes_departamento", 'class' => "form-control input-sm", 'style'=> "width:55%; margin-right: 5px"]) !!}	

						{!! Form::select('anio_departamento', $anios, date('Y'), ["id"=>"anio_departamento", 'class' => "form-control input-sm", 'style'=> "width:40%;"]) !!}						
						<div class="input-group-btn">
							<a href="javascript:void(0)" type="button" onclick="chartUpdateDepartemento()" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Actualizar Grafica"><i class="fa fa-refresh" aria-hidden="true"></i>
							</a>
						</div>

						<button class="btn btn-sm btn-default pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>

					{{-- {!! Form::close() !!} --}}
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="chart-incidentes-departamentos" style="height: 400px; width: 510px;" width="510" height="250"></canvas>
				</div>
			</div><!-- /.box-body -->
		</div><!-- /.box -->

	</div>

	<div class="col-md-6" >
		<!-- AREA CHART -->
		<div class="box box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">Incidentes por Departamentos Totales</h3>
				<div class="box-tools" >
					{{-- {!! Form::open() !!} --}}
					<div class="input-group pull-right" >						
						<div class="input-group-btn">
							<a href="javascript:void(0)" type="button" onclick="chartUpdateDepartementoTotales()" class="btn btn-sm btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Actualizar Grafica"><i class="fa fa-refresh" aria-hidden="true"></i>
							</a>
						</div>
						<button class="btn btn-sm btn-default pull-right" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>

					{{-- {!! Form::close() !!} --}}
				</div>
			</div>
			<div class="box-body">
				<div class="chart">
					<canvas id="chart-incidentes-departamentos-total" style="height: 400px; width: 510px;" width="510" height="250"></canvas>
				</div>
			</div><!-- /.box-body -->
			
		</div><!-- /.box -->

	</div>

	@endif

</div>
@endsection

@section('js')
<script src="{{ asset('plugins/chartjs2/Chart.min.js') }}"></script>
<script>
	var colores = [	'#4572A7','#AA4643','#89A54E','#80699B','#3D96AE',
	'#DB843D','#92A8CD','#A47D7C','#B5CA92','#058DC7',
	'#50B432','#ED561B','#DDDF00','#24CBE5','#64E572',
	'#FF9655','#FFF263','#6AF9C4','#CD5C5C','#FF0000',
	'#FF1493','#FF4500','#FFD700','#FFEFD5','#BDB76B',
	'#E6E6FA','#4B0082','#ADFF2F','#2E8B57','#66CDAA',
	'#00FFFF','#1E90FF','#000080','#B0C4DE','#FFDEAD',
	'#BC8F8F','#D2691E','#A52A2A','#2F4F4F','#FAEBD7',
	"#f45b5b", "#8085e9", "#8d4654", "#7798BF", "#aaeeee",
	"#ff0066", "#eeaaee","#55BF3B", "#DF5353", "#7798BF", 
	"#aaeeee"];

	var RUTA_ESTADITICAS = ruta+'/estadisticas';

	var chartUpdate = function(val){
		var data;
		//aler(RUTA_ESTADITICAS+'/incidentes/'+$('#mes').val()+'/'+$('#anio').val());
		$.ajax({
			url: RUTA_ESTADITICAS+'/incidentes/'+$('#mes').val()+'/'+$('#anio').val(),
			type: 'GET',
			dataType: 'json',
			success: function(res){ 
				var ctx = document.getElementById("chart-incidentes");
				var myChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: res.chart.labels,
						datasets: [{
							label: 'Incidentes',
							data: res.chart.data,
							backgroundColor: "rgba(96,189,104,1)",
							borderColor: "rgba(96,189,104,1)",
							pointBackgroundColor: "#fff",

						},{
							label: 'Incidentes Cerrados',
							data: res.chart.data_cerrado,
							backgroundColor: "#db8b0b",
							borderColor: "#db8b0b",
							pointBackgroundColor: "#fff",

						}]
					},
					options: {
						responsive: true, 
						maintainAspectRatio: false,
						scales: {
							xAxes: [{
								ticks: {
									fontSize: 10,
									fontStyle: 'bold'
								}
							}]
						}
					}
				});

				$('#total-incidentes').html(res.total_incidentes);
				$('#total-incidentes-cerrados').html(res.total_incidentes_cerrados);
				$('#total-incidentes-falta').html(res.total_incidentes_falta);
				$('#total-incidentes-proceso').html(res.total_incidentes_proceso);
				$('#total-incidentes-mes').html(res.total_incidentes_mes);

			},
			error: function(jqXHR, textStatus, errorThrown) {

			},
			complete: function(){
				$('#btn-load').prop('disabled', false);
				$('#btn-load i').removeClass('fa-pulse');
				$('#btn-load span').html('Actualizar');
			}
		});



	}

	var chartUpdateDepartemento = function(val){
		$.ajax({
			url: RUTA_ESTADITICAS+'/incidentes-departamentos/'+$('#mes_departamento').val()+'/'+$('#anio_departamento').val(),
			type: 'GET',
			dataType: 'json',
			success: function(res){ 
				var ctx = document.getElementById("chart-incidentes-departamentos");

				var data = {
					labels: res.chart.labels,
					datasets: [
					{
						data:res.chart.data,
						backgroundColor: colores,
						hoverBackgroundColor: colores
					}]
				};

				var myDoughnutChart = new Chart(ctx, {
					type: 'doughnut',
					data: data
				});

			},
			error: function(jqXHR, textStatus, errorThrown) {

			},
			complete: function(){
				$('#btn-load').prop('disabled', false);
				$('#btn-load i').removeClass('fa-pulse');
				$('#btn-load span').html('Actualizar');
			}
		});
	}

	var chartUpdateDepartementoTotales = function(val){
		$.ajax({
			url: RUTA_ESTADITICAS+'/incidentes-departamentos-total',
			type: 'GET',
			dataType: 'json',
			success: function(res){ 
				var ctx = document.getElementById("chart-incidentes-departamentos-total");

				var data = {
					labels: res.chart.labels,
					datasets: [
					{
						data:res.chart.data,
						backgroundColor: colores,
						hoverBackgroundColor: colores
					}]
				};

				var myDoughnutChart = new Chart(ctx, {
					type: 'doughnut',
					data: data
				});

			},
			error: function(jqXHR, textStatus, errorThrown) {

			},
			complete: function(){
				$('#btn-load').prop('disabled', false);
				$('#btn-load i').removeClass('fa-pulse');
				$('#btn-load span').html('Actualizar');
			}
		});
	}

	$(document).ready(function(){
		chartUpdate();
		chartUpdateDepartemento();
		chartUpdateDepartementoTotales()
	});
</script>
@endsection