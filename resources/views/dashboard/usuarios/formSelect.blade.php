<!-- Modal -->
<div class="modal fade" id="modal-usuario-select" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">
			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-usuario-select']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Formulario de usuarios</h4>
			</div>

			<div class="modal-body">

				<div class="row">
					<div class="form-group col-sm-6" id="field-departamento_id-select">
						{!! Form::label('departamento_id-select', 'Departamento:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::select('departamento_id-select', $deparatmentos, null,['id'=>'departamento_id-select', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div> 

					<div class="form-group col-sm-6" id="field-cargo-select">
						{!! Form::label('cargo-select', 'Cargo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('cargo-select', null, ['id'=>'cargo-select', 'class'=>'form-control','placeholder'=>'Cargo','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div> 
				</div>


				<div class="row">
					<div class="form-group col-sm-4" id="field-extension-select">
						{!! Form::label('extension-select', 'Extensión:',['class'=>'control-label']) !!}
						{!!Form::text('extension-select', null, ['id'=>'extension-select', 'class'=>'form-control','placeholder'=>'Extensión','required','data-inputmask'=>"'mask': '999'",'data-mask'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-4" id="field-rol_id-select">
						{!! Form::label('rol_id-select', 'Tipo de Usuario:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::select('rol_id-select', $roles, null, ['id'=>'rol_id-select', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-4" id="field-estado">
						{!! Form::label('estado-select', 'Estado:',['class'=>'control-label']) !!}
						{!!Form::select('estado-select',['0'=>'Inactivo', '1'=>'Activo'], null, ['id'=>'estado-select', 'class'=>'form-control','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>
				
				



			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-select" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>