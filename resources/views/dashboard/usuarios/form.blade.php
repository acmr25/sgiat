<!-- Modal -->
<div class="modal fade" id="modal-usuario" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">
			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-usuario']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Formulario de usuarios</h4>
			</div>

			<div class="modal-body">

				{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}
				{!! Form::hidden('id', null, ['id'=>'id']) !!} 

				<div class="row">
					<div class="form-group col-sm-6" id="field-nombre">
						{!! Form::label('name', 'Nombre:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('name', null, ['id'=>'name', 'class'=>'form-control','placeholder'=>'Nombre','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-6" id="field-apellido">
						{!! Form::label('apellido', 'Apellido:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('apellido', null, ['id'=>'apellido', 'class'=>'form-control','placeholder'=>'Apellido','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>

				<div class="form-group" id="field-email">
					{!! Form::label('email', 'Email:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
					{!!Form::email('email', null, ['id'=>'email', 'class'=>'form-control','placeholder'=>'example@example.com','required'])!!}
					<span>
						<strong class="text-danger msj-error"></strong>
					</span>
				</div> 

				<div class="row">
					<div class="form-group col-sm-6" id="field-id_departamento">
						{!! Form::label('id_departamento', 'Departamento:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::select('id_departamento', $deparatmentos, null,['id'=>'id_departamento', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div> 

					<div class="form-group col-sm-6" id="field-cargo">
						{!! Form::label('cargo', 'Cargo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::text('cargo', null, ['id'=>'cargo', 'class'=>'form-control','placeholder'=>'Cargo','required', 'onkeypress'=>'return soloLetras(event)'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-6" id="field-telefono">
						{!! Form::label('telefono', 'Telefono:',['class'=>'control-label']) !!}
						{!!Form::text('telefono', null, ['id'=>'telefono', 'class'=>'form-control','placeholder'=>'Telefono','required','data-inputmask'=>"'mask': '9999 999 9999'",'data-mask'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-6" id="field-extension">
						{!! Form::label('extension', 'Extensión:',['class'=>'control-label']) !!}
						{!!Form::text('extension', null, ['id'=>'extension', 'class'=>'form-control','placeholder'=>'Extensión','required','data-inputmask'=>"'mask': '999'",'data-mask'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

				</div>

				<div class="row">
					<div class="form-group col-sm-6" id="field-id_rol">
						{!! Form::label('id_rol', 'Tipo de Usuario:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!!Form::select('id_rol', $roles, null, ['id'=>'id_rol', 'class'=>'form-control','placeholder'=>'-Seleccione-','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-6" id="field-estado">
						{!! Form::label('estado', 'Estado:',['class'=>'control-label']) !!}
						{!!Form::select('estado',['0'=>'Inactivo', '1'=>'Activo'], null, ['id'=>'estado','placeholder'=>'-Seleccione-', 'class'=>'form-control','required'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-sm-6" id="field-password">
						{!! Form::label('password', 'Contraseña:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!! Form::password('password', ['id'=>'password', 'class'=>'form-control', 'placeholder'=>'*********','required']) !!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>

					<div class="form-group col-sm-6" id="field-password_confirmation">
						{!! Form::label('password_confirmation', 'Confirmar Contraseña:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
						{!! Form::password('password_confirmation', ['id'=>'password_confirmation', 'class'=>'form-control', 'placeholder'=>'*********','required']) !!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>



			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-usuario" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

