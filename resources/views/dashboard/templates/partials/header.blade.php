<header class="main-header">
	<!-- Logo -->
	<a href="{{ url() }}" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><img src="{{ asset('img/logo.png') }}" class="img-responsive" alt="Image"></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Amazonas</b>Tech</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">				
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('dist/img/person-icon-150x150.png') }}" class="user-image" alt="User Image">
						<span class="hidden-xs">{{ Auth::user()->name.' '.Auth::user()->apellido }}</span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="{{ asset('dist/img/person-icon-150x150.png') }}" class="img-circle" alt="User Image">
							<p>
								{{ Auth::user()->name.' '.Auth::user()->apellido }}
								<small>{{ Auth::user()->rol->nombre }}</small>
								
							</p>
						</li>
						
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="{{ url('perfil/index') }}" class="btn btn-default btn-flat pull-left">
									Perfil
								</a>
							</div>
							<div class="pull-right">
								<a href="{{ route('auth.logout') }}" class="btn btn-default btn-flat pull-right">
									Salir
								</a>
							</div>



						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>