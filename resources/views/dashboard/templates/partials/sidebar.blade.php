<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ asset('dist/img/person-icon-150x150.png') }}" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{ Auth::user()->name.' '.Auth::user()->apellido }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MENU</li>

			@if(Auth::user()->rol_id == 3)

			<li><a href="{{ url('usuarios') }}"><i class="fa fa-group  text-red"></i> <span>Gestión de Usuarios</span></a></li>

			

			<li class="{{Request::is('incidentesoporte')?'active':''}} treeview">
				<a href="#"><i class="fa fa-life-bouy text-yellow"></i> <span>Gestión de Incidentes</span> <i class="fa fa-angle-left pull-right"></i></a>
				<ul class="treeview-menu">

					<li><a href="{{ route('incidentesoporte.index') }}"><i class="fa fa-circle-o"></i> <span>Incidentes Pendientes</span></a></li>

					<li><a href="{{ url('incidentecerrados') }}"><i class="fa fa-circle-o"></i> <span>Incidentes Cerrados</span></a></li>
					
				</ul>
			</li>

			<li><a href="{{ url('mantenimiento') }}"><i class="fa fa-cog text-teal"></i> <span>Modulo de Mantenimiento</span></a></li>

			@else

			<li><a href="{{ route('incidentecliente.index') }}"><i class="fa fa-life-bouy text-yellow"></i> <span>Gestión de Incidentes</span></a></li>
			
			@endif
			<li><a href="{{ route('basesconocimientos.index') }}"><i class="fa fa-book text-aqua"></i> <span>Bases de Conocimiento</span></a></li>
			

			
		</ul>
	</section>
	<!-- /.sidebar -->
</aside> 