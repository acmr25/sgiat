<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SGIAT | Dashboard</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweealert/sweetalert2.min.css')}}">

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
      </head>
      <body class="hold-transition skin-green-light sidebar-collapse sidebar-mini">
       <div class="wrapper">

        {{-- header --}}
        @include('dashboard.templates.partials.header')
        {{-- header --}}


        {{-- sidebar --}}
        @include('dashboard.templates.partials.sidebar')
        {{-- sidebar --}}



        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
         <!-- Content Header (Page header) -->
         <section class="content-header">
          <h1>
           @yield('titulo_modulo')
           <small>@yield('descripcion_modulo')</small>
         </h1>
         <ol class="breadcrumb">
           <li><a href="{{ url() }}"><i class="fa fa-dashboard"></i> Home</a></li>
           @yield('breadcrumb')
         </ol>
       </section>
       <!-- Content Header (Page header) -->
       <section class="content-header">

        @yield('contenido')

      </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <footer class="main-footer">
     <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2017 - {{ date('Y') }} <a href="{{ url() }}"> SGIAT </a>.</strong> All rights reserved.
  </footer>


      <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
     $.widget.bridge('uibutton', $.ui.button);
   </script>

   <script type="text/javascript" src="{{asset('plugins/sweealert/promise.min.js')}}"></script>
   <script type="text/javascript" src="{{asset('plugins/sweealert/sweetalert2.min.js')}}"></script>
   
   <!-- Bootstrap 3.3.5 -->
   <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
   <!-- Slimscroll -->
   <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
   <!-- FastClick -->
   <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
   <!-- AdminLTE App -->
   <script src="{{ asset('dist/js/app.min.js') }}"></script>

   <!-- AdminLTE for demo purposes -->
   <script src="{{ asset('dist/js/demo.js') }}"></script>

   <script type="text/javascript">
     var leng = {
       "decimal":        "",
       "emptyTable":     "No hay datos disponibles en la tabla",
       "info":           "Mostrando  _START_ a _END_ de _TOTAL_ entradas ",
       "infoEmpty":      "Mostrando  0 a 0 de 0 entradas ",
       "infoFiltered":   "(filtered from _MAX_ total entradas )",
       "infoPostFix":    "",
       "thousands":      ",",
       "lengthMenu":     "Mostrar _MENU_ entradas ",
       "loadingRecords": "Loading...",
       "processing":     "Processing...",
       "search":         "Buscar:",
       "zeroRecords":    "No matching records found",
       "paginate": {
         "first":      "Primero",
         "last":       "Último",
         "next":       "Siguiente",
         "previous":   "Anterior"
       },
       "aria": {
         "sortAscending":  ": activate to sort column ascending",
         "sortDescending": ": activate to sort column descending"
       }
     };
     var ruta = "{{ url() }}";
   </script>
   @yield('js')
 </body>
 </html>

