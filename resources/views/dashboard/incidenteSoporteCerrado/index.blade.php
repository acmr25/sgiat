@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('titulo_modulo', "Incidentes Cerrados")

@section('contenido')
<div class="row">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	<div class="col-sm-12">
		<div class="box box-solid">
			
			<div class="box-body">
				<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar"><i class="fa fa-repeat"></i> Actualizar tabla</a>
				<hr>
				<table class="table table-bordered table-hover table-striped" id="tabla-incidentes" width="100%">
					<thead>
						{{-- <th  style="width: 32px; padding-right: 8px;">
							<input name="select_all" value="1" id="select-all" type="checkbox">
						</th> --}}
						<th style="width: 30px;">ID</th>
						{{-- aqui ira descripcion, problema, categoria y prioridad--}}
						<th style="width: 45%">Ticket</th> 
						<th>Solución</th>
						<th style="width: 100px;">Responsable</th>
                        <th style="width: 100px;">Calificación</th>
					</thead>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer">
			</div><!-- /.box-footer-->
		</div><!-- /.box -->


	</div>
</div>



@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/indicentesSoporteCierrado.js')}}"></script>
@endsection