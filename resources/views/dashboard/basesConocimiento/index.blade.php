@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<style type="text/css">
	table.dataTable thead .sorting_asc::after {
		content: none;
	}
</style>
@endsection

@section('titulo_modulo', "Bases de Conocimiento")

@section('breadcrumb')
<li class="active">Bases de Conocimiento</li>
@endsection

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="row">
	<div class="col-xs-12">	
		<div class="box box-solid">
			<div class="box-body">
				@include('flash::message')
				<form action="" method="POST" role="form">
					<div class="form-group">
						<div class="input-group">
							@if(Auth::user()->rol_id == 3)
							<div class="input-group-btn">

								<a type="button" class="btn btn-default" href="{{ route('basesconocimientos.create') }}"><i class="fa fa-plus"></i> Agregar Nuevo Tema</a>
							</div><!-- /btn-group -->
							@endif
							<input class="form-control input-xs" type="text" id="busrcar-tema">
							<div class="input-group-btn">
								<button type="button" class="btn btn-primary" onclick="cargar($('#busrcar-tema').val())">Buscar</button>
							</div><!-- /btn-group -->
						</div>
					</div>

				</form>
				<ul class="todo-list ui-sortable" id="tabla-ayuda">
				</ul>
				{{-- <table class="table table-hover" id="tabla-ayuda" width="100%">

			</table> --}}
		</hr>
	</div>
</div>
</div>
</div>


@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/basesConocimiento.js')}}"></script>


@endsection