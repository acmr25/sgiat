@extends('dashboard.templates.main')

{{-- @section('titulo_modulo', $conocimiento->titulo) --}}

@section('breadcrumb')
<li><a href="{{ route('basesconocimientos.index') }}">Bases de Conocimiento</a></li>
<li class="active">{{ $conocimiento->titulo }}</li>
@endsection

@section('contenido')
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<div class="row">
	<div class="col-xs-12">	
		<div class="box box-solid">
			<div class="box-body">
				<a href="{{ route('basesconocimientos.index') }}" class="btn btn-default btn-xs">
					<i class="fa fa-arrow-left" aria-hidden="true"></i> 
					Regresar
				</a>
				<h2>{{ $conocimiento->titulo }}</h2>
				<small>
					Categoria: {{ $conocimiento->categoria_nombre }} 
					@if(isset($conocimiento->sub_categoria_nombre)) 
					{{ ' > '.$conocimiento->sub_categoria_nombre }}
					@endif
				</small>
				<hr>
				<div class="contenido">
					{!! $conocimiento->descripcion !!}
				</div>
			</div>
			<div class="box-footer">
				<a href="{{ route('basesconocimientos.index') }}" class="btn btn-default btn-xs">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					Regresar
				</a>
			</div>
		</div>
	</div>
</div>
</div>


@endsection
