@extends('dashboard.templates.main')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-fileinput-master/css/fileinput.css') }}">

@endsection

@section('title_page', (isset($conocimiento->id)?'Editar Base de Conocimiento ('.$conocimiento->titulo.')':'Nuevo Base de Conocimiento') )

@section('breadcrumb')
<li class="active">Bases de Conocimiento</li>
@endsection 

@section('contenido')
<section class="content-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="box box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Nueva Base de Conocimiento</h3>
				</div>
				<div class="box-body pad">
					@include('flash::message')
					@if(isset($conocimiento->id))
					{!! Form::open(['route'=>['basesconocimientos.update',$conocimiento],'method'=>'PUT', 'files'=>true, 'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}
					{!! Form::hidden('id', $conocimiento->id) !!}  
					@else
					{!! Form::open(['route'=>'basesconocimientos.store','method'=>'POST', 'files'=>true, 'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}  
					@endif
					<br>
					{!! Form::hidden('_token', csrf_token(), ['id'=>'token']) !!}


					<div class="form-group{{ $errors->has('categoria_id')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('categoria_id', 'Categoria:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::select('categoria_id', $categorias ,(isset($conocimiento)?$conocimiento->categoria_id:null), ['id'=>'categoria_id', 'class'=>'form-control', 'placeholder'=>'-Seleccione-','required']) !!}
							@if($errors->has('categoria_id'))
							<span class="text-danger">
								<strong>{{ $errors->first('categoria_id')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div id="box-sub-categorias">
						
					</div>


					<div class="form-group{{ $errors->has('titulo')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('titulo', 'Titulo:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::text('titulo', (isset($conocimiento)?$conocimiento->titulo:null), ['id'=>'titulo', 'class'=>'form-control', 'placeholder'=>'titulo','required']) !!}
							@if($errors->has('titulo'))
							<span class="text-danger">
								<strong>{{ $errors->first('titulo')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group{{ $errors->has('descripcion')?' has-error':'' }}">
						<div class="col-md-3">
							<div class="pull-right">
								{!! Form::label('descripcion', 'Descripción:',['class'=>'control-label']) !!}<span style="color:red;"> *</span>
							</div>
						</div>
						<div class="col-md-9">
							{!! Form::textarea('descripcion', (isset($conocimiento)?$conocimiento->descripcion:null), ['id'=>'descripcion', 'class'=>'form-control', 'placeholder'=>'Descripcion','required', 'rows'=>3]) !!}
							@if($errors->has('descripcion'))
							<span class="text-danger">
								<strong>{{ $errors->first('descripcion')}}</strong>
							</span>
							@endif
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-md-12">
							<a href="{{route('basesconocimientos.index')}}" class="btn btn-warning btn-sm">Cancelar</a>
							{!! Form::submit('Guardar Cambios',['class'=>'btn btn-primary btn-sm pull-right']) !!}
						</div>
					</div>
					{!! Form::close() !!} 
				</div>
			</div>
		</div>
		
	</div>
</section>


@endsection

@section('js')

<script src="{{asset('plugins/bootstrap-fileinput-master/js/fileinput.js')}}"></script>
<script src="{{asset('plugins/bootstrap-fileinput-master/js/locales/es.js')}}"></script>
<script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>

<script type="text/javascript">


	CKEDITOR.replace('descripcion');

	$('#archivos').fileinput({
		showUpload: false,
		language: 'es'
	});

	function soloLetras(e){
		key = e.keyCode || e.which;
		tecla = String.fromCharCode(key).toLowerCase();
		letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
		especiales = "8-37-39-46";

		tecla_especial = false
		for(var i in especiales){
			if(key == especiales[i]){
				tecla_especial = true;
				break;
			}
		}

		if(letras.indexOf(tecla)==-1 && !tecla_especial){
			return false;
		}
	}

	function cargar(value){
		if(value != 0){
			$.ajax({
				url: ruta+'/categorias-by-padre/'+value,
				type: 'GET',
				success: function(res){ 
					var html = '<div class="form-group">'+
					'<div class="col-md-3">'+
					'<div class="pull-right">'+
					'<label class="control-label">Sub-categoria: <span style="color:red;"> *</span></label>'+
					'</div>'+
					'</div>'+
					'<div class="col-md-9">'+
					'<select name="sub_categoria" id="sub_categoria" class="form-control">';

					for(var i = 0; i<res.length; i++){
						html+='<option value="'+res[i].id+'">'+res[i].nombre+'</opction>';
					}

					html+='</select>'+
					'</div>'+
					'</div>';

					$('#box-sub-categorias').append(html);

					var sub_categoria = '{{ isset($conocimiento)?$conocimiento->sub_categoria:"" }}';

					if(sub_categoria != undefined && sub_categoria != null && sub_categoria != ""){
						$('#sub_categoria').val(sub_categoria);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					sweetAlert(
						'Error',
						'Ha ocurrido un error al tratar de obtener los datos de la categoria. Status: '+jqXHR.status,
						'error'
						)
				}
			});
		}else{
			$('#box-sub-categorias').html("");
		}
	}

	cargar($('#categoria_id').val());	

	$('#categoria_id').change(function(){
		cargar($(this).val());	
	});

</script>
@endsection