<!-- Modal -->
<div class="modal fade" id="modal-form-incidente" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog " role="document">
		<div class="modal-content box">

			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-incidente']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Incidentes</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-sm-12" id="field-id">
						{!! Form::label('id', 'ID:',['class'=>'control-label']) !!}
						{!!Form::text('id', null, ['id'=>'id', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-12" id="field-cliente">
						{!! Form::label('cliente', 'Cliente:',['class'=>'control-label']) !!}
						{!!Form::text('cliente', null, ['id'=>'cliente', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-6" id="field-categoria">
						{!! Form::label('categoria', 'Categoria:',['class'=>'control-label']) !!}
						{!!Form::text('categoria', null, ['id'=>'categoria', 'class'=>'form-control', 'readonly'])!!}
					</div> 

					<div class="form-group col-sm-6" id="field-sub_categoria">
						{!! Form::label('sub_categoria', 'Sub-Categoria:',['class'=>'control-label']) !!}
						{!!Form::text('sub_categoria', null, ['id'=>'sub_categoria', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>


				<div class="row">
					<div class="form-group col-sm-12" id="field-problema">
						{!! Form::label('problema', 'Problema:',['class'=>'control-label']) !!}
						{!!Form::text('problema', null, ['id'=>'problema', 'class'=>'form-control', 'readonly'])!!}
					</div>

					<div class="form-group col-sm-12" id="field-descripcion">
						{!! Form::label('descripcion', 'Descripcion:',['class'=>'control-label']) !!}
						{!!Form::textarea('descripcion', null, ['id'=>'descripcion', 'class'=>'form-control', 'readonly', 'rows'=>2])!!}
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-6" id="field-prioridad">
						{!! Form::label('prioridad', 'Prioridad:',['class'=>'control-label']) !!}
						{!!Form::text('prioridad', null, ['id'=>'prioridad', 'class'=>'form-control', 'readonly'])!!}
					</div> 

					<div class="form-group col-sm-6" id="field-fecha">
						{!! Form::label('fecha', 'Creado el:',['class'=>'control-label']) !!}
						{!!Form::text('fecha', null, ['id'=>'fecha', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-12" id="field-archivos">
						{!! Form::label('archivos', 'Archivos:',['class'=>'control-label']) !!}
						<span style="width: 100%" id="archivos-incidente"></span>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-sm-12" id="field-solucion">
						{!! Form::label('solucion', 'Solución:',['class'=>'control-label']) !!}
						{!!Form::textarea('solucion', null, ['id'=>'solucion', 'class'=>'form-control', 'rows'=>2])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div>
				</div>
				


			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-incidente" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>