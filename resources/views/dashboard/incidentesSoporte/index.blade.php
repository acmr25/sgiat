@extends('dashboard.templates.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">

<style type="text/css">
	.form-inline .form-control {
		display: inline-block;
		vertical-align: middle;
		width: none;
	}
	div.dataTables_filter input {
		width: 70%;
	}
	.titulo-incidente{
		font-weight: 900;
		text-transform: uppercase;
	}
	
</style>
@endsection

@section('titulo_modulo', "Incidentes")

@section('contenido')
<div class="row">
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

	<div class="col-sm-5">
		<div class="box box-solid">
			<div class="box-header text-center bg-green-active color-palette">
				<p class="box-title">Incidentes sin asignar</p>
			</div>
			
			<div class="box-body">
				<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar-incidentes"><i class="fa fa-repeat"></i> Actualizar tabla</a>
				<hr>
				<table class="table table-bordered table-hover table-striped" id="tabla-incidentes" width="100%">
					<thead>
						<th  style="width: 32px; padding-right: 8px;">
							<input name="select_all" value="1" id="select-all-incidentes" type="checkbox">
						</th>
						<th style="width: 10px;">ID</th>
						{{-- aqui ira descripcion, problema, categoria y prioridad--}}
						<th>Ticket</th> 
						<th class="hidden">Cliente</th> 
						<th class="hidden">Descripcion</th>
						<th class="hidden">Categoria</th>
						<th class="hidden">Priodirdad</th> 
					</thead>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer">
				<button class="btn btn-default btn-sm" id="btn-tomar-select"><i class="fa fa-share"></i> Tomar Seleccionados</button>
			</div><!-- /.box-footer-->
		</div><!-- /.box -->
	</div>

	<div class="col-sm-7">
		<div class="box box-solid">
			<div class="box-header text-center bg-green-active color-palette">
				<p class="box-title">Mis Incidentes</p>
			</div>
			<div class="box-body">
				<a href='javascript:void(0)' class="btn btn-default btn-sm"  id="actualizar-incidentes-mios"><i class="fa fa-repeat"></i> Actualizar tabla</a>
				<hr>
				<table class="table table-bordered table-hover table-striped" id="tabla-incidentes-mios" width="100%">
					<thead>
						{{-- <th  style="width: 32px; padding-right: 8px;">
							<input name="select_all" value="1" id="select-all-incidentes-mios" type="checkbox">
						</th> --}}
						<th style="width: 30px;">ID</th>
						{{-- aqui ira descripcion, problema, categoria y prioridad--}}
						<th style="width: 50%">Ticket</th> 
						<th style="width: 50%">Solución</th> 
						<th class="hidden">Cliente</th> 
						<th class="hidden">Descripcion</th>
						<th class="hidden">Categoria</th>
						<th class="hidden">Priodirdad</th>
					</thead>
				</table>
			</div><!-- /.box-body -->
			<div class="box-footer">
			</div><!-- /.box-footer-->
		</div><!-- /.box -->


	</div>
</div>
@include('dashboard.incidentesCliente.window')
@include('dashboard.incidentesSoporte.form')
@include('dashboard.incidentesSoporte.formTransferir')

@endsection

@section('js')
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script src="{{asset('js/validate.js')}}"></script>
<script src="{{asset('js/incidentesSoporte.js')}}"></script>
@endsection