<!-- Modal -->
<div class="modal fade" id="modal-form-incidente-transferir" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content box">

			<div class="modal-header text-center">
				{!! Form::open(['id'=>'form-incidente-transferir']) !!}
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Transferencia de Incidencia</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
					<div class="form-group col-sm-12" id="field-id-transferir">
						{!! Form::label('id-transferir', 'ID:',['class'=>'control-label']) !!}
						{!!Form::text('id-transferir', null, ['id'=>'id-transferir', 'class'=>'form-control', 'readonly'])!!}
					</div> 
				</div>

				<div class="row">
					<div class="form-group col-sm-12" id="field-reporte_id">
						{!! Form::label('reporte_id-transferir', 'Transferir:',['class'=>'control-label']) !!}
						{!!Form::select('reporte_id-transferir', $users, null, ['id'=>'reporte_id-transferir', 'class'=>'form-control','placeholder'=>'-Seleccione-'])!!}
						<span>
							<strong class="text-danger msj-error"></strong>
						</span>
					</div> 
				</div>



			</div>

			<div class="modal-footer">
				<button type="button" id="guardar-incidente-tranferencia" class="btn btn-primary btn-sm pull-left" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cerrar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>