<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidente extends Model
{
    protected $table = 'incidentes';

    protected $fillable = ['descripcion', 
    					   'problema', 
    					   'solucion', 
    					   'fecha_cierre', 
    					   'user_id', 
    					   'responsable_id', 
    					   'categoria_id', 
    					   'prioridad_id', 
    					   'estado_id',
                           'calificacion'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function responsable()
    {
    	return $this->belongsTo('App\User', 'responsable_id');
    }

    public function categoria()
    {
    	return $this->belongsTo('App\CategoriaIncidente', 'categoria_id');
    }

    public function prioridad()
    {
    	return $this->belongsTo('App\Prioridad', 'prioridad_id');
    }

    public function estado()
    {
    	return $this->belongsTo('App\EstadoIncidente', 'estado_id');
    }

    public function archivos()
    {
    	return $this->hasMany('App\Archivo');
    }
}
