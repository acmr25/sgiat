<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $table = "archivos";

    protected $fillable = [
    	'nombre_publico',
    	'nombre_interno',
    	'incidente_id'
    ];

    public function incidente()
    {
    	return $this->belongsTo('App\Incidente', 'incidente_id');
    }
}
