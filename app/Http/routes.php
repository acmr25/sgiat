<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'auth'],function(){

	Route::get('/', function () {
		$meses = [
		'01'=>'Enero', 
		'02'=>'Febrero', 
		'03'=>'Marzo', 
		'04'=>'Abril', 
		'05'=>'Mayo',
		'06'=>'Junio',
		'07'=>'Julio',
		'08'=>'Agosto',
		'09'=>'Septiembre',
		'10'=>'Octubre',
		'11'=>'Noviembre',
		'12'=>'Diciembre',
		];
		for ($i=2017; $i <= date('Y'); $i++) { 
			$anios[$i] = $i;
		}
		return view('dashboard.index')->with('meses', $meses)->with('anios', $anios);
	});

	

	Route::group(['middleware'=>'admin'],function(){
		Route::get('/mantenimiento', function() {
			return view('dashboard.mantenimiento.index');
		});

		Route::put('categorias/select', 'CategoriasController@updateSelect');
		Route::delete('categorias/select', 'CategoriasController@destroySelect');
		Route::get('categorias/catalago', 'CategoriasController@catalago');
		Route::get('categorias/listar', 'CategoriasController@listar');
		Route::resource('categorias', 'CategoriasController');

		


		Route::put('departamentos/select', 'DepartamentosController@updateSelect');
		Route::delete('departamentos/select', 'DepartamentosController@destroySelect');
		Route::get('departamentos/listar', 'DepartamentosController@listar');
		Route::resource('departamentos', 'DepartamentosController');

		Route::put('prioridades/select', 'PrioridadesController@updateSelect');
		Route::delete('prioridades/select', 'PrioridadesController@destroySelect');
		Route::get('prioridades/listar', 'PrioridadesController@listar');
		Route::resource('prioridades', 'PrioridadesController');

		Route::put('usuarios/select', 'UsuariosController@updateSelect');
		Route::delete('usuarios/select', 'UsuariosController@destroySelect');
		Route::get('usuarios/listar', 'UsuariosController@listar');
		Route::resource('usuarios', 'UsuariosController');

		Route::put('incidentesoporte/select', 'IncidenteSoporteController@updateSelect');
		Route::delete('incidentesoporte/select', 'IncidenteSoporteController@destroySelect');
		Route::get('incidentesoporte/listar/{option?}', 'IncidenteSoporteController@listar');
		Route::get('incidentesoporte/tomar/{id?}', 'IncidenteSoporteController@tomar');
		Route::put('incidentesoporte/tomarselect', 'IncidenteSoporteController@tomarSelect');

		Route::put('incidentesoporte/transferencia', 'IncidenteSoporteController@transferencia');
		Route::put('incidentesoporte/transferenciaselect', 'IncidenteSoporteController@transfereciaSelect');
		Route::resource('incidentesoporte', 'IncidenteSoporteController');

		Route::get('incidentecerrados', function(){
			return view('dashboard.incidenteSoporteCerrado.index');
		});

	});
    Route::get('categorias-by-padre/{id}', 'CategoriasController@categoriasByPadre');

	Route::put('incidentecliente/select', 'IncidentesClientesController@updateSelect');
	Route::delete('incidentecliente/select', 'IncidentesClientesController@destroySelect');
	Route::get('incidentecliente/listar', 'IncidentesClientesController@listar');
	Route::resource('incidentecliente', 'IncidentesClientesController');

	Route::get('basesconocimientos/listar/{cadena?}', 'BasesConocimintosController@listar');
	Route::resource('basesconocimientos', 'BasesConocimintosController');

	Route::get('estadisticas/incidentes/{mes?}/{anio?}', 'EstadisticasController@incidentes');
	Route::get('estadisticas/incidentes-departamentos/{mes?}/{anio?}', 'EstadisticasController@incidentesDepartamento');
	Route::get('estadisticas/incidentes-departamentos-total', 'EstadisticasController@incidentesDepartamentoTotal');

	Route::get('perfil/index',[
		'uses' => 'PerfilesController@index',
		'as' => 'perfil.index'
		]
		);
	Route::post('perfil/update/{user}',[
		'uses' => 'PerfilesController@update',
		'as' => 'perfil.update'
		]
		);

});

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', [
	'uses' => 'Auth\AuthController@postLogin',
	'as' => 'auth.login'
	]);
Route::get('logout', [
	'uses' => 'Auth\AuthController@getLogout',
	'as' => 'auth.logout'
	]);

// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');

Route::post('register', [
	'uses' => 'Auth\AuthController@postRegister',
	'as' => 'auth.register'
	]);


// Password reset link request routes...
Route::get('password/email', [
	'uses' => 'Auth\PasswordController@getEmail',
	'as'   => 'password.email.index'
	] );

Route::post('password/email', [
	'uses' => 'Auth\PasswordController@postEmail',
	'as'   => 'password.email'
	] );


Route::post('password/reset', [
	'uses' => 'Auth\PasswordController@postReset',
	'as'   => 'password.reset'
	] );