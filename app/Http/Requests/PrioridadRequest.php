<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrioridadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'nombre'=>'required|unique:prioridades,nombre,'.$this->id.',id',
            'tiempo'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es requerido',
            'nombre.unique'   => 'Este nombre ya se encuentra registrado',
            'tiempo.required' => 'El tiempo es requerido',
        ];
    }
}
