<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BaseConocimientoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'titulo' => 'required',
        'descripcion' => 'required'
        ];
    }

    public function messages()
    {
        return [
        'titulo.required' => 'El titulo es requerido',
        'descripcion.required' => 'El titulo es requerido'
        ];
    }
}
