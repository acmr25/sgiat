<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuarioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'email'                 => 'required|unique:users,email,'.$this->id.',id',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'name'                  => 'required|max:30|min:2|alpha',
            'apellido'              => 'required|max:30|min:2|alpha',
            'departamento_id'       => 'required',
            'cargo'                 => 'required',
            'rol_id'                => 'required'
        ];
        
    }
    
    public function messages()
    {
        return [
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            'name.required'                     => 'El Nombre es requerido',
            'name.max'                          => 'El Nombre no puede tener mas de 20 caracteres',
            'name.min'                          => 'El Nombre no puede tener menos de 2 caracteres',
            'name.alpha'                        => 'Este campo solo acepta letras',
            'apellido.required'                 => 'El Apellido es requerido',
            'apellido.max'                      => 'El Apellido no puede tener mas de 20 caracteres',
            'apellido.min'                      => 'El Apellido no puede tener menos de 2 caracteres',
            'apellido.alpha'                    => 'Este campo solo acepta letras',
            'email.unique'                      => 'Este Email ya se encuentra en uso',
            'email.email'                       => 'El Email debe de tener un formato ejemplo@ejemplo.com',
            'departamento_id.required'          => 'El Departameto es requerido',
            'cargo.required'                    => 'El Cargo es requerido',
            'rol_id.required'                   => 'El Tipo de usuario es requerido'
        ];
    }
}
