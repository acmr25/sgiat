<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use App\Http\Requests\UsuarioRequest;

use Auth;
use DB;
use Log;
use Exception;

use App\Departamento;
use App\Rol;
use App\User;

class PerfilesController extends Controller
{
    public function index()
    {
        return view('dashboard.perfil.form')
        ->with('user', Auth::user())
        ->with('deparatmentos', Departamento::lists('nombre', 'id'))
        ->with('roles', Rol::lists('nombre', 'id'));
    }

    
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate($request, [
            'email'                 => 'required|unique:users,email,'.Auth::user()->id.',id',
            'password'              => 'min:8|confirmed',
            'password_confirmation' => 'min:8',
            'name'                  => 'required|max:30|min:2|alpha',
            'apellido'              => 'required|max:30|min:2|alpha',
            'departamento_id'       => 'required',
            'cargo'                 => 'required',
            ],[
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            'name.required'                     => 'El Nombre es requerido',
            'name.max'                          => 'El Nombre no puede tener mas de 20 caracteres',
            'name.min'                          => 'El Nombre no puede tener menos de 2 caracteres',
            'name.alpha'                        => 'Este campo solo acepta letras',
            'apellido.required'                 => 'El Apellido es requerido',
            'apellido.max'                      => 'El Apellido no puede tener mas de 20 caracteres',
            'apellido.min'                      => 'El Apellido no puede tener menos de 2 caracteres',
            'apellido.alpha'                    => 'Este campo solo acepta letras',
            'email.unique'                      => 'Este Email ya se encuentra en uso',
            'email.email'                       => 'El Email debe de tener un formato ejemplo@ejemplo.com',
            'departamento_id.required'          => 'El Departameto es requerido',
            'cargo.required'                    => 'El Cargo es requerido',
           
            ]);

        DB::beginTransaction();
        try {
            $usuario = User::findOrFail(Auth::user()->id);

            $pass_last = $usuario->password;

            $usuario->fill($request->all());

            if($request->password != null && !empty($request->password)){
                $usuario->password = bcrypt($request->password);
            }else{
                $usuario->password = $pass_last;
            }
            $usuario->save();

            DB::commit();
            Flash::success("Se han actualizados sus datos  exitosamente!");
            return redirect()->route('perfil.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            Flash::error("Ha ocurrido un error al intentar guardar los datos, vuelva a intentar más tarde.");
            return redirect()->route('perfil.index');
        }
    }

    
}
