<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;

use App\Incidente;



class EstadisticasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incidentes($mes, $anio)
    {
        $incidentes = Incidente::orderBy('dia', 'ASC')
        ->select(
            DB::raw('COUNT(*) as contador'), 
            DB::raw('DATE_FORMAT(created_at, "%d/%m/%Y") AS dia')
                            // DB::raw("(SELECT COUNT(*) FROM incidentes AS i2 WHERE i2.estado_id = 3 AND DATE_FORMAT(i2.updated_at, '%d / %m / %Y') = dia) AS cerrado")
            )
        ->where(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), $anio.'-'.$mes)
        ->groupBy(DB::raw('DAY(created_at)'))
        ->orderBy(DB::raw('DAY(created_at)'), 'ASC')
        ->get();


        $incidentes_cerrados = Incidente::orderBy('dia', 'ASC')
        ->select(
            DB::raw('COUNT(*) as contador'), 
            DB::raw('DATE_FORMAT(updated_at, "%d/%m/%Y") AS dia'))
        ->where(DB::raw('DATE_FORMAT(updated_at, "%Y-%m")'), $anio.'-'.$mes)
        ->where('estado_id', 3)
        ->groupBy(DB::raw('DAY(updated_at)'))
        ->orderBy(DB::raw('DAY(updated_at)'), 'ASC')
        ->get();


        $numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
        $cont = 0;
        $data_cerrado = array();
        for ($i=1; $i <= $numero; $i++) { 
            $aux = 0;
            $aux2 = 0;

            for ($j=0; $j < count($incidentes); $j++) { 
                if($incidentes[$j]->dia == date('d/m/Y', strtotime($i.'-'.date('m-Y')))){
                    $data[] = $incidentes[$j]->contador;
                    $aux = 1;
                    break;
                }
            }

            for ($j=0; $j < count($incidentes_cerrados); $j++) { 
                if($incidentes_cerrados[$j]->dia == date('d/m/Y', strtotime($i.'-'.date('m-Y')))){
                    $data_cerrado[] = $incidentes_cerrados[$j]->contador;
                    $aux2 = 1;
                    break;
                }
            }

            $labels[] = date('d/m/Y', strtotime($i.'-'.date('m-Y')));

            if($aux == 0){
                $data[] = 0;
            }

            if($aux2 == 0){
                $data_cerrado[] = 0;
            }
        }

        $chart = ['labels'=>$labels, 'data'=>$data, 'data_cerrado'=>$data_cerrado];

        $total_incidentes = Incidente::count();

        $total_cerrados = Incidente::where('estado_id', 3)
        ->count();

        $total_incidentes_mes = Incidente::where(DB::raw('DATE_FORMAT(created_at, "%Y-%m")'), $anio.'-'.$mes)
        ->count();

        $total_falta = Incidente::where('estado_id', 1)
        ->count();

        $total_proceso = Incidente::where('estado_id', 2)
        ->count();
            //dd($total_incidentes);
        return response()->json([
            'chart' => $chart, 
            'total_incidentes'=> $total_incidentes, 
            'total_incidentes_mes'=> $total_incidentes_mes,
            'total_incidentes_cerrados' => $total_cerrados,
            'total_incidentes_falta' => $total_falta,
            'total_incidentes_proceso' => $total_proceso
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function incidentesDepartamento($mes, $anio)
    {
        $incidentes = Incidente::orderBy('departamento', 'ASC')
        ->select(
            DB::raw('COUNT(*) as contador'), 
            'departamentos.nombre AS departamento'
            )
        ->join('users', 'incidentes.user_id', '=', 'users.id')
        ->join('departamentos', 'users.departamento_id', '=', 'departamentos.id')
        ->where(DB::raw('DATE_FORMAT(incidentes.created_at, "%Y-%m")'), $anio.'-'.$mes)
        ->groupBy('departamento')
        ->get();

        $labels = [];
        $data = [];   

        for ($j=0; $j < count($incidentes); $j++) { 
            $labels[] = $incidentes[$j]->departamento;
            $data[] = $incidentes[$j]->contador;

            
        }


        $chart = ['labels'=>$labels, 'data'=>$data];

        return response()->json([
            'chart' => $chart
            ]);

    }

    public function incidentesDepartamentoTotal()
    {
        $incidentes = Incidente::orderBy('departamento', 'ASC')
        ->select(
            DB::raw('COUNT(*) as contador'), 
            'departamentos.nombre AS departamento'
            )
        ->join('users', 'incidentes.user_id', '=', 'users.id')
        ->join('departamentos', 'users.departamento_id', '=', 'departamentos.id')
        ->groupBy('departamento')
        ->get();

        $labels = [];
        $data = [];   

        for ($j=0; $j < count($incidentes); $j++) { 
            $labels[] = $incidentes[$j]->departamento;
            $data[] = $incidentes[$j]->contador;
        }


        $chart = ['labels'=>$labels, 'data'=>$data];

        return response()->json([
            'chart' => $chart
            ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
