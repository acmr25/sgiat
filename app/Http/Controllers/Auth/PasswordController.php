<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

use Illuminate\Http\Request;

use Laracasts\Flash\Flash;

use Log;
use Exception;
use DB;
use Auth;

use App\User;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function getEmail()
    {
        return view('dashboard.auth.passwords.email');
    }

    public function postEmail(Request $request)
    {

        try {
            $user = User::where('email', $request->email)->firstOrFail();
            return view('dashboard.auth.passwords.resect')->with('user', $user);
        } catch (\Exception $e) {
            Flash::error('El email que ingreso no se encuentra registrado!');
            return redirect()->route('password.email.index');
        }
    }

    public function postReset(Request $request)
    {

        $this->validate($request, [
            'password'              => 'min:8|required|confirmed',
            'password_confirmation' => 'min:8|required',
            ],[
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            ]);

        DB::beginTransaction();
        try {
            $user = User::findOrFail($request->id);
            $user->password = bcrypt($request->password);
            $user->save();
            Auth::login($user);
            DB::commit();
            Flash::success("Se han actualizados sus datos  exitosamente!");
            return redirect()->route('perfil.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            Flash::error("Ha ocurrido un error al intentar guardar los datos, vuelva a intentar más tarde.");
            return view('dashboard.auth.passwords.resect')->with('user', $user);
        }
    }
}
