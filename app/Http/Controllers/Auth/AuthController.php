<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

use Laracasts\Flash\Flash;

use DB;
use Log;

use App\User;
use App\Departamento;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath  = '/';
    
    protected $loginPath    = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function getLogin()
    {
        return view('dashboard.auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 
            'password' => 'required',
        ],[
            'email.required' => 'El Email es requerido',
            'password.required' => 'Debe de ingresar la Contraseña'
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);

        
    }

    public function getRegister()
    {
        return view('dashboard.auth.register')->with('departamentos', Departamento::lists('nombre', 'id'));
    }

    // public function postRegister(Request $request)
    // {
    //     $validator = $this->validator($request->all());

    //     if ($validator->fails()) {
    //         $this->throwValidationException(
    //             $request, $validator
    //         );
    //     }

    //     Auth::login($this->create($request->all()));

    //     return redirect($this->redirectPath());
    // }

    public function postRegister( Request $request)
    {
        $rules = [
            'email'                 => 'required|unique:users,email',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'name'                  => 'required|max:30|min:2|alpha',
            'apellido'              => 'required|max:30|min:2|alpha',
            'departamento_id'       => 'required',
            'cargo'                 => 'required'
        ];
        
        $messages = [
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            'name.required'                     => 'El Nombre es requerido',
            'name.max'                          => 'El Nombre no puede tener mas de 20 caracteres',
            'name.min'                          => 'El Nombre no puede tener menos de 2 caracteres',
            'name.alpha'                        => 'Este campo solo acepta letras',
            'apellido.required'                 => 'El Apellido es requerido',
            'apellido.max'                      => 'El Apellido no puede tener mas de 20 caracteres',
            'apellido.min'                      => 'El Apellido no puede tener menos de 2 caracteres',
            'apellido.alpha'                    => 'Este campo solo acepta letras',
            'email.unique'                      => 'Este Email ya se encuentra en uso',
            'email.email'                       => 'El Email debe de tener un formato ejemplo@ejemplo.com',
            'email.required'                    => 'El Email es requerido',
            'departamento_id.required'          => 'El Departameto es requerido',
            'cargo.required'                    => 'El Cargo es requerido'
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        DB::beginTransaction();
        try {
                
            
            if($validator->fails())
            {
                return redirect('register')->withErrors($validator)->withInput();
            }
            else
            {
                $user = new User($request->all());
                $user->rol_id = 2;
                $user->estado = 1;
                $user->password = bcrypt($request->password);
                $user->remember_token = str_random(100);
                
                $user->save();

            }
            
            Auth::login($user);
            DB::commit();
            return redirect($this->redirectPath());
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en AuthController. Msj: '.$e->getMessage().', Linea: '.$e->getLine().', Archivo: '.$e->getFile());
            Flash::error('Ha ocurrido un error al tratar de registrarlo');
            return redirect('register');
        }
    }

   
    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param  array  $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|max:255',
    //         'email' => 'required|email|max:255|unique:users',
    //         'password' => 'required|confirmed|min:6',
    //     ]);
    // }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return User
    //  */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => bcrypt($data['password']),
    //     ]);
    // }
}
