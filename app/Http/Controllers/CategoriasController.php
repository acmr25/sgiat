<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CategoriaRequest;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\CategoriaIncidente;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $request)
    {
        DB::beginTransaction();
        try {
            $categoria = new CategoriaIncidente($request->all());
            $categoria->save();
            DB::commit();
            return response()->json($categoria);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $categoria = CategoriaIncidente::findOrFail($id);
            return response()->json($categoria);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $categoria = CategoriaIncidente::findOrFail($id);
            if($categoria->id == $request->padre){
                $padre = $categoria->padre; 
            }else{
                $padre = $request->padre;
            }
            $categoria->fill($request->all());
            $categoria->padre = $padre;
            $categoria->save();
            DB::commit();
            return response()->json($categoria);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function updateSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $categoria = CategoriaIncidente::findOrFail($id);
                if($categoria->id == $request->padre){
                    $padre = $categoria->padre; 
                }else{
                    $padre = $request->padre;
                }
                $categoria->fill($request->all());
                $categoria->padre = $padre;
                $categoria->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $categoria = CategoriaIncidente::findOrFail($id);
            CategoriaIncidente::where('padre', $id)->update(['padre'=>'']);
            $categoria->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function destroySelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $categoria = CategoriaIncidente::findOrFail($id);
                CategoriaIncidente::where('padre', $id)->update(['padre'=>'']); 
                $categoria->delete();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }


    public function listar()
    {
        try {
            $categorias = CategoriaIncidente::get();
            $categorias->each(function($categorias){
                if($categorias->padreData != null){
                    $categorias->padre = $categorias->padreData->nombre;
                }else{
                    $categorias->padre = "";
                }
                return $categorias;
            });
            return Datatables::of($categorias)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function catalago()
    {
        try {
            $categorias = CategoriaIncidente::orderBy('padre')->get();

            return response()->json($categorias);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }

    public function categoriasByPadre($id)
    {
        try {
            $categorias = CategoriaIncidente::where('padre', $id)->get();
            return response()->json($categorias);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CategoriasController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }
}
