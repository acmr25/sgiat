<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\IncidenteRequest;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\Incidente;
use App\CategoriaIncidente;
use App\EstadoIncidente;
use App\Prioridad;
use App\Archivo;
use App\User;

class IncidenteSoporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('id',DB::raw('CONCAT(name," ",apellido) AS nombre'))->where('rol_id', 3)->where('id', '!=',Auth::user()->id)->lists('nombre','id');


        return view('dashboard.incidentesSoporte.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
        $prioridades = Prioridad::lists('nombre', 'id');
        return view('dashboard.incidentesSoporte.form')->with('categorias', $categorias)->with('prioridades', $prioridades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncidenteRequest $request)
    {

        DB::beginTransaction();
        try {
            $incidente = new Incidente($request->all());

            if(isset($request->sub_categoria)){
                $categoria = $request->sub_categoria;
            }else{
                $categoria = $request->categoria_id;
            }
            $incidente->categoria_id = $categoria;
            $incidente->user_id = Auth::user()->id;
            $incidente->estado_id = 1; // 1 es Abierto y debe de permanecer asi
            
            $incidente->save();

            foreach ($request->archivos as $i) {
                $nombre_publico = $i->getClientOriginalName();
                $extension = $i->getClientOriginalExtension();
                $nombre_interno = str_replace('.'.$extension, '',$nombre_publico);
                $nombre_interno = str_slug($nombre_interno, '-').'-'.time().'-'.strval(rand(100,999)).'.'.$extension;
                $path_file = public_path().'/uploads/archivo';

                $archivo = new Archivo([
                    'nombre_publico' => $nombre_publico,
                    'nombre_interno' => $nombre_interno,
                    'incidente_id' =>  $incidente->id
                    ]);
                $archivo->save();
                $i->move($path_file,$nombre_interno);
                
            }
            DB::commit();
            Flash::success('Se ha creado su incidencia satisfactoriamente, será resuelta a la brevedad.');
            return redirect()->route('incidentecliente.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            Flash::error('Ha ocurrido un error al tratar de registrar su incidencia.');
            $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
            $prioridades = Prioridad::lists('nombre', 'id');
            return view('dashboard.incidentesSoporte.form')
            ->with('incidente', $request)
            ->with('categorias', $categorias)
            ->with('prioridades', $prioridades);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->user;
            $incidente->responsable;

            if($incidente->categoria->padre == null ){
                $incidente->categoria_principal = $incidente->categoria->nombre;
                $incidente->sub_categoria = "";
            }else{
                $categoria_padre = CategoriaIncidente::find($incidente->categoria->padre);
                $incidente->categoria_principal = $categoria_padre->nombre;
                $incidente->sub_categoria = $incidente->categoria->nombre;
            }
            $incidente->fecha = date('d/m/Y h:i:s A', strtotime($incidente->created_at));
            $incidente->categoria;
            $incidente->prioridad;
            $incidente->estado;
            $incidente->archivos;
            return response()->json($incidente);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IncidenteRequest $request, $id)
    {
        $this->validate($request, [
            'solucion'                 => 'required',
            ],[
            'solucion.required'    => 'Este campo es obligatorio',
            ]);
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->fill($request->all());
            $estado = EstadoIncidente::where('nombre', 'RESPONDIDO')->first();
            $incidente->estado_id = $estado->id;
            $incidente->save();
            DB::commit();
            return response()->json($incidente);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function updateSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);               
                $incidente->fill($request->all());
                $incidente->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function destroySelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);
                $incidente->delete();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }


    public function listar($option = 1)
    {
        try {
            // $incidentes = Incidente::orderBy('id');
            switch ($option) {
                case 1:
                    $incidentes = Incidente::whereNull('responsable_id')->get();
                break;
                
                case 2:
                    $incidentes = Incidente::where('responsable_id', Auth::user()->id)->where('estado_id', '!=', 3)->get();
                break;

                case 3:
                    $incidentes = Incidente::where('estado_id', 3)->get();
                break;
            }

            $incidentes->each(function($incidentes){
                $incidentes->cliente = $incidentes->user->name.' '.$incidentes->user->apellido;
                $incidentes->responsable;
                $incidentes->categoria;
                $incidentes->prioridad;
                $incidentes->estado;
                $incidentes->archivos;
                $incidentes->fecha = date('d/m/Y h:i:s A', strtotime($incidentes->created_at));
                return $incidentes;
            });

            for ($i=0; $i < count($incidentes); $i++) { 
                # code...
            }
            return Datatables::of($incidentes)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function tomar($id)
    {
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->responsable_id = Auth::user()->id;
            $estado = EstadoIncidente::where('nombre', 'EN PROCESO')->first();
            $incidente->estado_id = $estado->id;
            $incidente->save();
            DB::commit();
            return response()->json($incidente);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }

    public function transferencia(Request $request)
    {
        $this->validate($request, [
            'responsable_id'                 => 'required',
            ],[
            'responsable_id.required'    => 'Este campo es obligatorio',
            ]);
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($request->id);
            $incidente->responsable_id = $request->responsable_id;
            $incidente->save();
            DB::commit();
            return response()->json($incidente);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }

    public function tomarSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);               
                $incidente->responsable_id = Auth::user()->id;
                $estado = EstadoIncidente::where('nombre', 'EN PROCESO')->first();
                $incidente->estado_id = $estado->id;
                $incidente->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function tranferirSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);               
                $incidente->responsable_id = $request->user_id;
                $incidente->save();
            }
            DB::commit();
            return response()->json($incidente);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidenteSoporteController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }

}
