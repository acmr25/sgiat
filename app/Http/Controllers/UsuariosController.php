<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use App\Http\Requests\UsuarioRequest;

use Auth;
use DB;
use Log;
use Exception;

use App\Departamento;
use App\Rol;
use App\User;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.usuarios.index')
        ->with('deparatmentos', Departamento::lists('nombre', 'id'))
        ->with('roles', Rol::lists('nombre', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email'                 => 'required|unique:users,email,'.$request->id.',id',
            'password'              => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
            'name'                  => 'required|max:30|min:2|alpha',
            'apellido'              => 'required|max:30|min:2|alpha',
            'departamento_id'       => 'required',
            'cargo'                 => 'required',
            'rol_id'                => 'required'
            ],[
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            'name.required'                     => 'El Nombre es requerido',
            'name.max'                          => 'El Nombre no puede tener mas de 20 caracteres',
            'name.min'                          => 'El Nombre no puede tener menos de 2 caracteres',
            'name.alpha'                        => 'Este campo solo acepta letras',
            'apellido.required'                 => 'El Apellido es requerido',
            'apellido.max'                      => 'El Apellido no puede tener mas de 20 caracteres',
            'apellido.min'                      => 'El Apellido no puede tener menos de 2 caracteres',
            'apellido.alpha'                    => 'Este campo solo acepta letras',
            'email.unique'                      => 'Este Email ya se encuentra en uso',
            'email.email'                       => 'El Email debe de tener un formato ejemplo@ejemplo.com',
            'email.required'                    => 'El Email es requerido',
            'departamento_id.required'          => 'El Departameto es requerido',
            'cargo.required'                    => 'El Cargo es requerido',
            'rol_id.required'                   => 'El Tipo de usuario es requerido'
            ]);

        DB::beginTransaction();

        try {
            $usuario = new User($request->all());
            $usuario->password = bcrypt($request->password);
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $usuario = User::findOrFail($id);
            return response()->json($usuario);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email'                 => 'required|unique:users,email,'.$request->id.',id',
            'password'              => 'min:8|confirmed',
            'password_confirmation' => 'min:8',
            'name'                  => 'required|max:30|min:2|alpha',
            'apellido'              => 'required|max:30|min:2|alpha',
            'departamento_id'       => 'required',
            'cargo'                 => 'required',
            'rol_id'                => 'required'
            ],[
            'password_confirmation.required'    => 'Este campo es requerido',
            'password.required'                 => 'Este campo es requerido',
            'password.confirmed'                => 'Las contraseña no coinciden vuelva a intentar',
            'password.min'                      => 'La contraseña debe de tener minimo 8 caracteres',
            'name.required'                     => 'El Nombre es requerido',
            'name.max'                          => 'El Nombre no puede tener mas de 20 caracteres',
            'name.min'                          => 'El Nombre no puede tener menos de 2 caracteres',
            'name.alpha'                        => 'Este campo solo acepta letras',
            'apellido.required'                 => 'El Apellido es requerido',
            'apellido.max'                      => 'El Apellido no puede tener mas de 20 caracteres',
            'apellido.min'                      => 'El Apellido no puede tener menos de 2 caracteres',
            'apellido.alpha'                    => 'Este campo solo acepta letras',
            'email.unique'                      => 'Este Email ya se encuentra en uso',
            'email.email'                       => 'El Email debe de tener un formato ejemplo@ejemplo.com',
            'departamento_id.required'          => 'El Departameto es requerido',
            'cargo.required'                    => 'El Cargo es requerido',
            'rol_id.required'                   => 'El Tipo de usuario es requerido'
            ]);

        DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);

            $pass_last = $usuario->password;

            $usuario->fill($request->all());

            if($request->password != null && !empty($request->password)){
                $usuario->password = bcrypt($request->password);
            }else{
                $usuario->password = $pass_last;
            }
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function updateSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $usuario = User::findOrFail($id);               
                if(!empty($request->departamento_id)){
                    $grupo->departamento_id = $request->departamento_id;
                } 
                if(!empty($request->cargo)){
                    $grupo->cargo = $request->cargo;
                } 
                if(!empty($request->extension)){
                    $grupo->extension = $request->extension;
                } 
                if(!empty($request->estado)){
                    $grupo->estado = $request->estado;
                } 
                if(!empty($request->rol_id)){
                    $grupo->rol_id = $request->rol_id;
                } 
                $usuario->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $usuario->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function destroySelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $usuario = User::findOrFail($id);
                $usuario->delete();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }


    public function listar()
    {
        try {
            $usuarios = User::get();
            $usuarios->each(function($usuarios){
                $usuarios->name =  $usuarios->name.' '.$usuarios->apellido;
                $usuarios->departamento;
                $usuarios->rol;
                return $usuarios;
            });
            return Datatables::of($usuarios)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsuariosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
}
