<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DepartamentoRequest;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\Departamento;

class DepartamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartamentoRequest $request)
    {
        DB::beginTransaction();
        try {
            $departamento = new Departamento($request->all());
            $departamento->save();
            DB::commit();
            return response()->json($departamento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $departamento = Departamento::findOrFail($id);
            return response()->json($departamento);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartamentoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $departamento = Departamento::findOrFail($id);
            $departamento->fill($request->all());
            $departamento->save();
            DB::commit();
            return response()->json($departamento);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function updateSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $departamento = Departamento::findOrFail($id);               
                $departamento->fill($request->all());
                $departamento->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $departamento = Departamento::findOrFail($id);
            $departamento->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function destroySelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $departamento = Departamento::findOrFail($id);
                $departamento->delete();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }


    public function listar()
    {
        try {
            $departamento = Departamento::get();
            return Datatables::of($departamento)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en DepartamentosController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

}
