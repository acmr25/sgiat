<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\BaseConocimientoRequest;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\BaseConocimiento;
use App\CategoriaIncidente;


class BasesConocimintosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.basesConocimiento.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
        return view('dashboard.basesConocimiento.form')->with('categorias', $categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BaseConocimientoRequest $request)
    {
        // dd($request->all());
        DB::beginTransaction();
        try {
            $bc = new BaseConocimiento($request->all());

            if(isset($request->sub_categoria)){
                $categoria = $request->sub_categoria;
            }else{
                $categoria = $request->categoria_id;
            }
            $bc->categoria_id = $categoria;
            
            $bc->save();

            DB::commit();
            Flash::success('Se ha creado un nuevo tema como base de conocimiento!');
            return redirect()->route('basesconocimientos.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en BasesConocimintosController. Mensaje: '.$e->getMessage().', Archivo: '.$e->getFile().', Linea: '.$e->getLine());
            Flash::error('Ha ocurrido un error al tratar de crear un nuevo como base de conocimiento.');
            $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
            return view('dashboard.basesConocimiento.form')
            ->with('conocimiento', $request)
            ->with('categorias', $categorias);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        try {
            $bc = BaseConocimiento::where('slug',$slug)->firstOrFail();

            if($bc->categoria->padreData != null){
                $bc->sub_categoria_nombre = $bc->categoria->nombre;
                $bc->categoria_nombre = $bc->categoria->padreData->nombre;
            }else{
                $bc->categoria_nombre = $bc->categoria->nombre;
            }
            return view('dashboard.basesConocimiento.show')
            ->with('conocimiento', $bc);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en BasesConocimintosController. Mensaje: '.$e->getMessage().', Archivo: '.$e->getFile().', Linea: '.$e->getLine());
            Flash::error('El tema al que desea acceder no se ha encontrado.');
            return redirect()->route('basesconocimientos.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $bc = BaseConocimiento::findOrFail($id);

            if($bc->categoria->padreData != null){
                $bc->sub_categoria = $bc->categoria_id;
                $bc->categoria_id = $bc->categoria->padreData->id;
            }
            // dd($bc);
            $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
            return view('dashboard.basesConocimiento.form')
            ->with('conocimiento', $bc)
            ->with('categorias', $categorias);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en BasesConocimintosController. Mensaje: '.$e->getMessage().', Archivo: '.$e->getFile().', Linea: '.$e->getLine());
            Flash::error('Ha ocurrido un error al tratar de crear un nuevo como base de conocimiento.');
            
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BaseConocimientoRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $bc = BaseConocimiento::findOrFail($id);
            $bc->fill($request->all());
            if(isset($request->sub_categoria)){
                $categoria = $request->sub_categoria;
            }else{
                $categoria = $request->categoria_id;
            }
            $bc->categoria_id = $categoria;
            
            $bc->save();

            DB::commit();
            Flash::success('Se ha actualizado tema con exito!');
            return redirect()->route('basesconocimientos.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en BasesConocimintosController. Mensaje: '.$e->getMessage().', Archivo: '.$e->getFile().', Linea: '.$e->getLine());
            Flash::error('Ha ocurrido un error al tratar de editar el Tema!');
            $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
            return view('dashboard.basesConocimiento.form')
            ->with('conocimiento', $request)
            ->with('categorias', $categorias);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $bc = BaseConocimiento::findOrFail($id);
            $bc->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en BasesConocimintosController. Mensaje: '.$e->getMessage().', Archivo: '.$e->getFile().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function listar($cadena = "")
    {
        try {

            if($cadena == ""){
                $bcs = BaseConocimiento::get();
                
            }else{
                $bcs = BaseConocimiento::join('categorias_incidentes', 'bases_conocimientos.categoria_id', '=', 'categorias_incidentes.id')
                ->where('bases_conocimientos.titulo', 'like', "%".$cadena."%")
                ->orWhere('categorias_incidentes.nombre', 'like', "%".$cadena."%")
                ->get();
                
            }
            
            $bcs->each(function($bcs){
                if($bcs->categoria->padreData != null){
                    $bcs->sub_categoria_nombre = $bcs->categoria->nombre;
                    $bcs->categoria_nombre = $bcs->categoria->padreData->nombre;
                }else{
                    $bcs->categoria_nombre = $bcs->categoria->nombre;
                }

                $bcs->rol_id = Auth::user()->rol_id;

                return $bcs->categoria;
            });

            return response()->json($bcs);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en HelpsController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([]);
        }
    }
}
