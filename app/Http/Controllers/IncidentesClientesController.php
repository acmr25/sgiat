<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\IncidenteRequest;

use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use Auth;
use DB;
use Log;
use Exception;

use App\Incidente;
use App\CategoriaIncidente;
use App\EstadoIncidente;
use App\Prioridad;
use App\Archivo;

class IncidentesClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.incidentesCliente.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
        $prioridades = Prioridad::lists('nombre', 'id');
        return view('dashboard.incidentesCliente.form')->with('categorias', $categorias)->with('prioridades', $prioridades);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncidenteRequest $request)
    {

        DB::beginTransaction();
        try {
            $incidente = new Incidente($request->all());

            if(isset($request->sub_categoria)){
                $categoria = $request->sub_categoria;
            }else{
                $categoria = $request->categoria_id;
            }
            $incidente->categoria_id = $categoria;
            $incidente->user_id = Auth::user()->id;
            $incidente->estado_id = 1; // 1 es Abierto y debe de permanecer asi
            
            $incidente->save();

            foreach ($request->archivos as $i) {
                if($i != null){
                    $nombre_publico = $i->getClientOriginalName();
                    $extension = $i->getClientOriginalExtension();
                    $nombre_interno = str_replace('.'.$extension, '',$nombre_publico);
                    $nombre_interno = str_slug($nombre_interno, '-').'-'.time().'-'.strval(rand(100,999)).'.'.$extension;
                    $path_file = public_path().'/uploads/archivo';

                    $archivo = new Archivo([
                        'nombre_publico' => $nombre_publico,
                        'nombre_interno' => $nombre_interno,
                        'incidente_id' =>  $incidente->id
                        ]);
                    $archivo->save();
                    $i->move($path_file,$nombre_interno);
                }
            }
            DB::commit();
            Flash::success('Se ha creado su incidencia satisfactoriamente, será resuelta a la brevedad.');
            return redirect()->route('incidentecliente.index');
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            Flash::error('Ha ocurrido un error al tratar de registrar su incidencia.');
            $categorias = CategoriaIncidente::where('padre', 0)->lists('nombre', 'id');
            $prioridades = Prioridad::lists('nombre', 'id');
            return view('dashboard.incidentesCliente.form')
            ->with('incidente', $request)
            ->with('categorias', $categorias)
            ->with('prioridades', $prioridades);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->user;
            $incidente->responsable;
            $incidente->categoria;
            $incidente->prioridad;
            $incidente->estado;
            $incidente->archivos;
            return response()->json($incidente);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IncidenteRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->calificacion = $request->calificacion;
            $estado = EstadoIncidente::where('nombre', 'CERRADO')->first();
            $incidente->estado_id = $estado->id;
            $incidente->save();
            DB::commit();
            return response()->json($incidente);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    public function updateSelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);               
                $incidente->fill($request->all());
                $incidente->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $incidente = Incidente::findOrFail($id);
            $incidente->delete();
            DB::commit();
            return response()->json($id);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }

    public function destroySelect(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $incidente = Incidente::findOrFail($id);
                $incidente->delete();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de optener los datos.'
                ],404);
        }
    }


    public function listar()
    {
        try {
            $incidentes = Incidente::where('user_id', Auth::user()->id)->get();
            $incidentes->each(function($incidentes){
                $incidentes->user;
                $incidentes->responsable;
                $incidentes->categoria;
                $incidentes->prioridad;
                $incidentes->estado;
                $incidentes->archivos;
                return $incidentes;
            });
            return Datatables::of($incidentes)->make(true);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en IncidentesController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
}
