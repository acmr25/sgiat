<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaIncidente extends Model
{
    protected $table = 'categorias_incidentes';

    protected $fillable = ['nombre', 'descripcion', 'padre'];

    public function padreData()
    {
    	return $this->belongsTo('App\CategoriaIncidente', 'padre');
    }

    public function basesConocimiento()
    {
    	return $this->hasMany('App\BaseConocimiento');
    }

    public function incidentes()
    {
    	return $this->hasMany('App\Incidente');
    }
}
