<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{
    protected $table = "prioridades";

    protected $fillable = ['nombre', 'tiempo', 'descripcion'];

    public function incidente()
    {
    	return $this->hasMany('App\Incidente');
    }
}
