<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class BaseConocimiento extends Model implements SluggableInterface
{
	use SluggableTrait;
    
    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
    ];

    protected $table = 'bases_conocimientos';

    protected $fillable = [
    	'titulo',
    	'descripcion',
    	'categoria_id',
    ];

    public function categoria()
    {
    	return $this->belongsTo('App\CategoriaIncidente', 'categoria_id');
    }
}
