<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoIncidente extends Model
{
    protected $table = 'estados_incidentes';

    protected $fillable = ['nombre', 'descripcion'];

    public function incidente()
    {
    	return $this->hasMany('App\Incidente');
    }
}
